
# Neat Notes

- fluorinated compounds don’t contribute to ozone
    depletion



- fluorine too eager to bind to other
    things
- does have high GHG index

## Core Electrons

- electrons in filled inner levels
- doesn’t affect bonding

## Stratosphere

- not much window or mixing
- verystratified
    (hehe…)

## Solubility

- The reason that nonpolar sticks with nonpolar
    isn’t because nonpolar-nonpolar interactions are stronger than
    polar-nonpolar but because polar-polar is so much stronger.



- polar-polar \> polar-nonpolar \>
    nonpolar-nonpolar

## Laws of Thermodynamics

- 1st: energy neither created nor destroyed



- you can’t win



- 2nd:
    ΔSuniv \> 0
    for all spontaneous processes (and all non-spontaneous processes are
    driven by some spontaneous process)



- everything sucks and is inefficient



- 3rd:
    ΔSuniv is
    only constant at absolute zero



- absolute zero is the only time entropy
    doesn’t
