# Chapter 0: Class Policies

## The Syllabus

### Main Class Info

  - Exams: no makeup, other exams reweighed iff
    excused
  - HW: precedes class
  - Attendance: required and part of grade
  - Educational Help



  - SI
  - UTC
  - instructor
  - problem session
  - study group

### Main Tools

  - Wolfware (Moodle): general
  - Webassign: homework
  - Tophat: in class

## Mental Health

  - Imposter Syndrome: feel like you don’t belong
    when you do (feel like imposter)
  - Stereotype Threat: feel extreme pressure to do
    something in order to avoid “contributing to stereotype”
