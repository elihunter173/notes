
# Chapter 4: Ionic Bonds

## Section 4.1: Ionic Bonding

### Type of Bonds

- Ionic Bond: complete
    transfer of electrons. makes anion and cation, which are then
    loosely attracted together



- stark differences in electronegativities. Coulombic
    forces

### How Ionic Bonds Work

- Less Electronegative: loses electron to more
    electronegative. becomes cation (+) (because electron lower energy
    there)
- More Electronegative: steals electron from less
    electronegative. becomes anion (-) (because electron lower energy
    here)
- Cation and Anion then loosely bind together in
    lattice/crystal (lowers total energy



- there's no true connection\! just a bunch of
    loose-ish connections

### General Trends

- Metals lose electrons → cations (+)
- Nonmetals gain electrons → anions
(-)

![](media/imported/image132.png)

- beryllium actually normally makes covalent bonds
    because of high
electronegativity

#### Examples

![](media/imported/image150.png)

- it’s in Na and Cl’s “best interest” to
    swap



- if it wasn’t high enough, they’d share (covalent
    bonding)

### Electron Configurations of Ions

- atoms always want most stable configuration
    (“normal” configuration is the most stable
    one)
- Non-metals: gains electrons (normally), highest
    electronegativity atom gains electrons first (feed the hungriest
    first)
- Metals: gives electrons first (normally), lowest
    ionization energy loses electrons first (free the roudiest
    most)

## Section 4.2: Naming Ions & Predicting Charge

### Terms

- Ionic Compound: a compound made up of anion and
    cation
- Oxidation Stage: what the charge of a single
    atom is



- depends when in compound, otherwise atomic
    charge

###

![](media/imported/image144.png)![](media/imported/image126.png)

### Naming

#### Cation

- Suffix = + “ion” to show that it’s a
    cation
- Only One Oxidation State: cation\_name
    anion\_name



- Ex:



- NaClO4 =
    sodium
    perchlorate
- Ca3N2 =
    calcium
    nitride
- K2SO4 =
    potassium sulphate
- ZnS = zinc sulphide



- Multiple Oxidation State:
    cation\_name(oxidation\_state) anion\_state



- oxidation state is in Roman numerals. figure
    charge of cation from anion
    charge
- Ex:



- FeSO4 =
    Iron(II)
    Sulphate
- FeCl3 =
    Iron(III) Chloride
- VO = Vanadium(II)
    Oxide
- RuPO4 =
    Ruthenium(III) Phosphate

#### Anion

- Suffix = “-ide” to show that its an anion

### Predicting Lost Electrons

#### Rules

1.  least electronegative / lowest ionization energy
    loses first
2.  lose highest sublevel \`n\` electrons



- Fe: \[Ar\]
    3d6 4s2



- Fe2+:
    \[Ar\]
    3d6
- Fe3+:
    \[Ar\]
    3d5[\[k\]](#cmnt11)



3.  lose highest \`l\` electrons

#### General Trends

- Group 1A = +1 orbital oxidation
- Group 2A = +2 orbital oxidation
- Group 3A = +3 orbital oxidation



- Tl = +1 or +3 orbital oxidation



- Group 4A = +2 orbital oxidation



- losing s-block (making +4) is too hard



- Transition Metals = +2 (or +3 and sometimes +1)
    orbital oxidation



- lose s-block easily and then sometimes like
    losing a few from d-block

#### Exceptions

- Ag = +1 only
- Cu = +1 or +2

### Predicting Gained Electrons

#### Rules

1.  most electronegative gains first
2.  gain in terms of energy (normal)
3.  gain until full octet reached (or similar stable
    state)

#### General Trends

- Group 7A = -1 orbital oxidation
- Group 6A = -2 orbital oxidation
- Group 5A = -3 orbital oxidation

## Section 4.3: Atomic vs Ionic Size

- Cations: smaller than atom



- because higher
    Zeff (less
    electrons = less shielding)



- Anions: larger than atom



- because lower
    Zeff (more
    electrons = more shielding)

## Section 4.4: Oxidation States

- Oxidation States: the
    charge an atom would have if its bonds
    wereionic



- oxidation state, formal charge : everything
    perfectly ionic, everything perfectly covalent

### Rules

- more electronegative element “takes” electron
    and less electronegative element “loses” electron



- when equal, both oxidation state is 0



- all oxidations states in compound sum to
    compound’s charge
- atoms want to fill their
octet

## Section 4.5: Polyatomic Ions

![](media/imported/image187.png)
