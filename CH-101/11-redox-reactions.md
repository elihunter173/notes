# Chapter 11: Redox Reactions

* **Electrochemistry:** The study of how chemical reactions and electricity interact.

## Quick Review

* **Oxidation Number:** What you get when you compute the number of electrons as if every bond is perfectly ionic (i.e. electrons are transferred)

$$\sum ON = charge$$
$$EN_X = EN_Y \therefore ON_X = 0, ON_Y = 0$$
$$EN_X > EN_Y \therefore ON_X = 1, ON_Y = -1$$
$$EN_X < EN_Y \therefore ON_X = -1, ON_Y = 1$$

![](media/imported/image182.png)

## Section 11.1: Electron Transfer (aka. Redox Reactions)

* **Redox Reaction:** A reaction where both oxidation and reduction occur. A reaction where electrons are transferred.
  * **Oxidation:** The loss of electrons or increase in electron number. (Oxidation Number $\downarrow$ : Electrons $\uparrow$)
    * _Why oxidation?_ Because oxidation number increases.
  * **Reduction:** The gain of electrons or decrease in electron number. (Oxidation Number $\uparrow$ : Electrons $\downarrow$)
    * _Why reduction?_ Because oxidation number decreases.
* **Oxidizing Agent / Oxidant:** Electron acceptor, being reduced. Is oxidized at first, but "gives" its oxidation to another atom.
  * Often metals.
* **Reducing Agent / Reductant:** Electron producer, being oxidized. Is reduced at first, but "gives" its reduction to another atom.
  * Often metals.
* _The reaction is extensive if the reaction produces weaker oxidizing and reducing agents than it had to start out with._
  * Because, otherwise, it would happen more often in the reverse.
* **Donor Orbitals:** The orbital that initially contains the electrons to be given. In a higher orbital than acceptor orbital.
  * On the reducing agent.
  * Strong reducing agents have very high energy donor orbitals. _Electrons wanna leave!_
* **Acceptor Orbitals:** The orbital that will contain the redox electrons. In a lower orbital than acceptor orital.
  * On the oxidizing agent.
  * Strong oxidizing agents have very low energy acceptor orbitals. _Electrons love it!_
* **Redox Electrons:** The electrons initally in the donor orbitals.
* **Redox Couple:** An oxidized form of an atom/molecule and a reduced version of it.
  * e.g. ($Cu^{2+}/Cu$, $Fe^{2+}/Fe$, etc.)

### Examples

$$Cu^{2+} + Fe \rightarrow Cu + Fe^{2+}$$

### Tips

* Group 7A elements can be reduced to -1 or oxidized to +7.
* Ignore (H) and (O) normally.
* Oxidizing/Reducing agents can be either atoms or molecules, _BUT_ when determining them, consider each individual atom.
* You can't oxidize something without reducing another. It's a zero sum game!
* Electrons lost and gained must balance out.
  * Consider the coefficient of the atoms/molecules.
* Strong acids are oxidizing agents (because (H^+))

## Section 11.2: Half Reactions

## Electrochemical Cells

* Write redox equations as two half equations of reduction and oxidation. _It makes it easier to balance and more realistic._
  * $Fe(s) + Cu^{2+} \leftrightharpoons Fe^{2+} + Cu(s)$
  * The two half equations...
  * $Fe(s) \leftrightharpoons Fe^{2+}(aq) + 2e^-$
  * $Cu^{2+} \leftrightharpoons Cu(s) + 2e^-$
  * You can actually have redox reactions occurring through a wire because the electron can go down a wire.
* $E*{reduction} = -E*{oxidation}$

## Summing Half reactions

1.  Identify the oxidation and reduction parts.
2.  Reverse the tabulated reduction half to match the oxidation expected.
3.  Determine the LCM for the number of electrons and use that to balance the two half-equaitons.
4.  Add the two half-reactions.

## Section 11.3: Galvanic Cells

* **Galvanic Cells:** Chemical Energy $\rightarrow$ Electrical Energy. Spontaneous.
* Electrons move towards more positive areas. $V > 0$
* **Faraday's Constant:** The charge of a mole of electrons. $F = 96,500\ C/mol$
* **Voltage:** The electrical potential difference between two things. $E\_{cell}\ J/C$
  * Work done per coulomb
* Each electrode (cathode and anode) has an individual _half-cell potential_.
* **Standard Cell Potential:** $E^0*{cell} = E^0*{cathode} - E^0\_{anode}$ because electrons start in the anode and end in the cathode.
  * Normal one just has no 0 in the superscript.
* **Work:** $W = -\Delta G = nE\_{cell}F$
* **Standard Free Energy:** $\Delta G^0 = -nE\_{cell}F^0$
  * Extensive if $\Delta G^0 < 0 \therefore E*{cathode}^0 > E*{anode}^0$

![Energy Level Illustration](media/11-redox-reactions/energy-level-spontaneity.png)

### Parts of a Galvanic Cell

![Galvanic Cell](media/11-redox-reactions/galvanic-cell.png)

* Each electrode compartment contains an electrode of a metal immersed in a solution of that metal.
* **Active Electrode:** Are the reactants and participate in the reaction.
* **Passive Electrode:** Provide simply the surface for the reaction.
* **Anode:** The half cell where oxidation occurs / electrons are released. Starts out reduced. The more negative $E\_{cell}$ wants to be the anode.
* **Cathode:** The half cell where reduction occurs / electrons are absorbed. Starts out oxidized. The more positive $E\_{cell}$ wants to be the cathode.
* **Liquid Junction / Salt Bride:** Maintains charge balance by being a strong electrolyte that can flow between the two electrode compartments as needed.
  * _cation $\rightarrow$ cathode_
  * _anion $\rightarrow$ anode_
* **Load:** Something that uses the electricity generated by the galvanic cell. Can be anything, a voltometer (above), lightbulb, toy, etc.
  * _If a voltometer is negative, then it is the wrong way around_

## Section 11.4: Standard Reduction Potentials

* Voltage is completely relative. Therefore, to measure things the hydrogen electrode at standard conditions is set to be 0V.
  * The measured electrode is connected to the "hi" end of the voltometer (aka. as if it was a cathode or reduction).
* Larger voltage means spontaneous.
  * Flipping order of reaction flips the sign of the voltage.
* **Standard reduction potentials are independent of the number of electrons transferred.**

$$E^0_{cell} = E^0_{cathode} - E^0_{anode}$$
_N.B. This is where both are measured as reduction. The anode is subtracted because it's actually oxidation._

## Section 11.5: Writing Redox Reactions

* **Standard Form:** $Red_1 + Ox_2 \rightarrow Ox_1 + Red_2$
* **Notation for Galvanic Cells:** One `|` for every junction between two compartments.
  * e.g. A single `|` between a solution and an electrode, but two `|` between the two main electrode compartments.

## Section 11.6: Batteries

* Batteries are just galvanic cells

### Alkaline Batteries

* **Anode:** $Zn \rightarrow Zn^{2+} + 2 e^-$
* **Cathode:** $2 MnO_2 + H_2O + 2 e^- \rightarrow Mn_2O_3 + 2 OH^-$
* $E_{cell} = 1.5V$

### Silver (Button) Batteries

* **Anode:** $Zn \rightarrow Zn^{2+} + 2 e^-$
* **Cathode:** $Ag_2O + H_2O + 2 e^- \rightarrow 2 Ag + 2 OH^-$
  * Sometimes: $HgO + H_2O + 2 e^- \rightarrow Hg + 2 OH^-$
* $E_{cell} = 1.6V$
  * Sometimes (with $Hg$): $E_{cell} = 1.6V$

### Lead Acid / Automobile Batteries

* **Anode:** $Pb + SO_4^{2-} \rightarrow PbSO_4  + 2 e^-$
* **Cathode:** $PbO_2 + 4 H^+ + SO_4^{2-} + 2 e^- \rightarrow 2 PbSO_4 + H_2O$
* $E_{cell} = 2.0V$
* _Benefit: Rechargable_

## Section 11.7: Corrosion

* **Corrosion:** Undesired oxidation of metal.
  * Needs (acidic) water and oxygen.
* **Anode:** $Fe \rightarrow Fe^{2+} + 2 e^-$
* **Cathode:** $4 H^+ + O_2 + 4 e^- \rightarrow 2 H_2O$
  * Why oxygen and acidic water is needed.
* $E_{cell} = 1.67V$
* Then $Fe^{2+}$ is oxidized by oxygen to $Fe^{3+}$ which then becomes $Fe_2O_3$.

### Prevention

* **Galvanization:** Plating the metal in a more reactive metal, thus protecting the metal you actually care about.
  * $Zn$ is often used to protect $Fe$.
* **Passivation:** Cover the metal in an (any) oxide layer that can't be further oxidized.

## Section 11.8: Electrolytic Cells

![Galvanic vs Electrolytic Illustration](media/11-redox-reactions/galvanic-vs-electrolytic.png)

* **Electrolytic Cells:** Electrical Energy $\rightarrow$ Chemical Energy. Non-spontaneous, require a driving force.
  * This works because cells with a higher energy are more stable at higher energies. Electrolytic cells just force them to be at this higher energy.
* Negative $E_{cell}$ shows the amount of work that is _required_ to force the reaction.
* **Electrolysis:** The process of forcing a reaction.
  * Used to separate water molecules, to plate ores, to separate ore from slag, etc.
* _When batteries are discharging, they are galvanic._
* _When batteries are recharging, they are electrolytic._
