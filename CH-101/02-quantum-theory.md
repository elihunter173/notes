# Chapter 2: Quantum Theory

## Section 2.1: Light / Electromagnetic (EM) Radiation

![Electromagnetic Spectrum](media/02-quantum-theory/em-spectrum.png)

## Section 2.2: Quantization

* **Quantization:** Continuous -> discrete. All energy comes in packets (photons)
  * *Issues this Solved:* Blackbody radiation, the pohotoelectric effect, why does amplitude only change electron count.
    * Blackbody Radiation: Things emit em radiation based off temperature.
      * Wave energy was thought to only change ampilitude which implies...
        * All wavelengths are emitted equally.
        * There are an infinite number of possible waves.
        * An infinite amount of emergy was created.
    * Photoelectric Effect: Metals irradiate em radiation with KE based only off the metal and a frequency (v).
  * *Solution Idea:* em radiation is made up of independent particle-waves (photons). The sum/count of these is intensity. However, since each particle hits independently, only their own KE (frequency, v) matters.
    * Blackbody Radiation: There is no longer an infinite number of possible waves.
    * Photoelectric Effect: All metals bind electrons with an energy (W), so when $E_{photon} = hv \geq W$, the metal's electron absorbs $W$ amount of energy and breaks its bond, leaving with the rest of the the $E_{photon} = hv$.
      * $KE_{electron} = hv - W = hv - hv_0$



### Line Spectrums

* Rydberg Equations show how much energy
    off
* gas discharge tubes emit specific ν based off
    element

## Section 2.3: Bohr Atomic Model

* Bohr Atomic Model: electrons orbit in discrete
    orbits using quantized angular
momentums

![](media/imported/image142.jpg)

### Problem before Quantization

* Coulomb’s Law: why don’t
    electrons spiral into nucleus?



* only a few orbits are stable

### Quantized Angular Momentum Solution

* angular momentum of electron is quantized
    (limited, discrete number) and is proportional with n



* eg: electrons orbit in
    discreteenergy levels



* n: principal quantum number (angular momentum) /
    the energy level, non-zero integer
* Coulomb’s Law: explains why electrons don’t spin
    in

### Energy Levels

* Energy Level: the same as angular momentum, but
    easier to understand
* Electronic Transition: when an electron changes
    energy
    levels
* ΔEatom changed
    by Ephoton



* Absorption:
    Ephoton received
    and
    ΔEatom\>0



* nlo →
    nhi
* non-spontaneous :’(



* Emission:
    Ephoton given
    off and
    ΔEatom\



* nhi →
    nlo
* spontaneous\!



* Ephoton determines
    frequency and thus color (can only give off a single photon at a
    time)
* BOHR MODEL ONLY WORKS WITH HYDROGEN-LIKE
    ELEMENTS (1 electron)

## Section 2.4: Quantum Theory

* Electrons = wave-particles



* Louis deBroglie discovered



* Electron Density: probability of finding an
    electron in a certain area



* arises because of wave-particle nature

## Section 2.5: The Shrodinger Model

* Production of Model: apply particle-wave idea to
    electrons (using wave functions) and treat electrons like tiny
    waves



* electrons need 4 numbers to describe their waves
    (the quantum numbers\!)



* Conceptualization: electrons can only exist in
    where there is a stable configuration for their wavelength (standing
    wave\!)

![](media/imported/image122.jpg)

* ## Energy Level: how many standing waves there are / nodes + 1

* Wave Functions: ???



* squared wave function is probability of finding
    electron in the area

### The 4 Quantum Numbers

* n: principal — level (int \> 0)



* primary indicator of energy of an
    electron
* shows average distance from nucleus



* l: orbital angular
    momentum — sublevel (int ≥ 0; l \



* determines what shape the orbital is
* 0, 1, 2, 3, 4, 5, 6, …, n
* s, p, d,
    f[\[d\]](#cmnt4),g,
    h, i[\[e\]](#cmnt5),
    …



* ml:
    magnetic orbital — orbital (int; -l ≤
    ml ≤
    +l)



* determines which “lobe”/“axis” of the sublevel
    the electron orbits
    in



* ms:
    spin (+½ ↑ clockwise, -½ ↓ counterclockwise)



* think clockwise or counterclockwise generating
    fields either upwards or downwards
* can only fit 2
    e- per
    orbital

#### Concepts from Quantum Numbers

* Energy = n + l



* generally energy
    increased with n, then l (if 2 sublevels same, put lower n
    first)



* 3p \
* 3d empty \> 4s
* 3d with
    e- \<
    4s



* Sublevel = n & l
* Orbital = n & l &
    ml



* Size = n
* Shape = l &
    ml



* Nodes = l
* Lobes =
2l

![](media/imported/image173.png)

![](media/imported/image178.png)

## Section 2.6: Orbital Shapes & Sizes

* Orbital = n & l &
    ml



* Size = n
* Shape = l
* Orientation = ml

### Electron Probability Regions (shapes)

* THESE ARE JUST VISUALIZATIONS OF WAVE FUNCTIONS
    AT VARIOUS POINTS
* colors show what node lobe is which (l)
* different orientations show magnetic orbital
    (ml)



![](media/imported/image175.png)

![](media/imported/image159.png)[\[f\]](#cmnt6)

![](media/imported/image180.png)

* Nodes: where wave function / orbital = 0
* Nodal Plane: planes that intersect wave function
    where wave function = 0



* l defines the number of nodal planes



* Orbitals (Wave Functions): regions of amplitude
    ±



* 1 orbital can have multiple regions (look at p
    above)

## Section 2.7: Electron Configuration

### Rules for Electron Configuration

* electrons enter orbital with lowest energy
    available
* Pauli Exclusion
    Principle: no two electrons can have the
    same 4 quantum numbers



* orbitals can only fit 2 electron in them

### Electron Configuration

#### Writing

* Long Form: nl\# of
    electrons



* Si:
    1s2 2s2 2p6 3s2 3p2
* Ge:
    1s2 2s2 2p6 3s2 3p6 3d10 4s2 4p2



* Noble Gas Form: \[X\] \<additional electrons not
    in X\>



* where X is noble gas
* Bi: \[Kr\]
    5s2 4d10 5p5

#### Chart

![](media/imported/image164.png)

* illustrates all the electrons and their quantum
    numbers
* Hund’s Rule: the
    lowest energy arrangement of electrons maximizes the number with the
    same spin



* electrons like to spread out and don’t pair up
    until they “have to”

### Electron Energy States

#### Atomic

* Ground State: lowest possible energy
    state



* unpaired electrons pointing same
    direction
* no empty/half-filled orbitals below a
    half-filled/full orbital[\[g\]](#cmnt7)



* Excited State: a possible energy state that
    isn’t the lowest



* neitherground nor
    not possible



* Not Possible: doesn’t follow Pauli Exclusion
    Principle (no identical electrons)

#### Orbital

* Paired Electrons / Filled Orbital: electrons
    with different spin in same orbital (higher energy)
* Unpaired Electrons / Half-filled Orbital:
    electron that is alone in orbital (lower energy)

#### D-Block Energy Oddities

* Energy: 3d \
* Fill Order: 4s → 3d



* because 3d has less energy only when
    filled



* 1 electron goes into d shell before f shell
    fills



* after f, rest of shell fills

#### (Our) Irregular Ground States

* because difference between d and s sub-level so
    slight
* Ground Cr —
    3s1 3d5



* because it doesn’t want to have a paired
    s



* Ground Cu —
    4s1 3d10



* because it doesn’t want to have a paired
    s

#### Photoelectron Spectroscopy

* how we know the order of electron
    orbitals
* Method: shoot x-rays at atoms and knock off
    electrons and measure their
    energy



* Eelectron =
    x-ray - energy of
shell

## Section 2.8: Quantum Theory & The Periodic Table

![](media/imported/image120.png)

* the periodic table follows quantum
    theory\!
