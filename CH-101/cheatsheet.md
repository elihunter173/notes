# Cheatsheet

## Variables

- E: energy (J)
- KE: kinetic energy (J)
- U: potential energy (J)
- q: charge (C)

### Electromagnetic

- λ: wavelength, peak-to-peak distance (m)



- λ ↑ = E ↓
- λ ↓ = E ↑



- ν: frequency, number of oscillations per second,
    (s-1)



- ν ↑ = E ↑
- ν ↓ = E
    ↓



- A/n:intensity
    of wave / number of photons (wave-particle duality\!)



- A/n ↑ = E ↑
- A/n ↓ = E
    ↓



- ν0(X):
    minimum frequency for metal to exhibit photoelectric effect
    (s-1)

### Bohr Model

- n: energy level / principal quantum number
    (m)
- rn:
    radius at principal quantum
     (m)

### Electron Shielding

- Zeff:
    how strong the charge of an atom is felt from the position of an
    electron



- ie: how strong does
    itreally feel

# Constants

### Coulomb’s Laws

- q: single unit of charge, 1.602 *
    10-18 C
- k: Coulomb’s constant, 8.9875 *
    109 N-m2-C-2
- ε: dielectric constant, varies based on
    medium



- shows how well the medium shields charges

### Electromagnetic

- c: speed of light, 2.998 *
    108 m/s



- ν ↔ λ



- h: Planck’s constant, 6.626 *
    10-34 J-s



- ν ↔ E



- R: Rydberg constant, 3.290
    *
    1015 s-1



- nlevel ↔
    ν ↔
    E

### Bohr Model

- a0:
    Bohr radius, most likely distance between nucleus and electron of H
    at ground state, 5.292 *
    10-11 m

### Gas

- R: ideal gas law constant, .0821
    L-atm-mol-1-K-1
- STP: standard temperature and pressure, 273 K, 1
    atm, 1 mole

### Thermal Energy

- ![](media/imported/image41.png)

# Functions

- ![](media/imported/image103.png)
- ![](media/imported/image47.png)



- ![](media/imported/image48.png): energy
    absorbed/gained
- ![](media/imported/image49.png): energy
    emitted/lost

### Coulomb’s Laws

- Coulomb’s Laws



- ![](media/imported/image104.png)



- F \
- F \> 0 is repulsive



- ![](media/imported/image105.png)



- really relative to energy at r ∞ (ie: not
    interacting), but that is 0

### Electromagnetic

- ![](media/imported/image106.png)
- ![](media/imported/image107.png)
- ![](media/imported/image108.png)



- n photons (amplitude\!)



- ![](media/imported/image109.png)



- T: transmittance
- I:
    intensity
- I0:
    intensity initial



- ![](media/imported/image110.png)



- A: absorbance
- I:
    intensity
- I0:
    intensity initial

### Bohr Model

- Rydberg Equations for Hydrogen-like Elements (1
    electron)



- ![](media/imported/image111.png)
- ![](media/imported/image112.png)
- nlo \<
    nhi are
    positive integers (energy levels / principal quantum number)



- ![](media/imported/image113.png)
- ![](media/imported/image114.png)
- ![](media/imported/image115.png)



- — b/c energy decreases as electrons approach
    nucleus



- ![](media/imported/image116.png)

### Applications of Zeff

- ![](media/imported/image117.png)



- σ: inner-sublevel electrons



- ![](media/imported/image8.png)
- ![](media/imported/image9.png)
- ![](media/imported/image118.png)



- ![](media/imported/image10.png)
- negative orbital energy

### Molecular Forces

- ![](media/imported/image42.png)



- μ: dipole moment (kinda
    like strength of “molecular magnet”)
- q: magnitude of partial
    charge
- d: distance between
    opposite charges









[\[a\]](#cmnt_ref1)atoms are legos\!





[\[b\]](#cmnt_ref2)energy is always relative





[\[c\]](#cmnt_ref3)the t is like a "+"\!





[\[d\]](#cmnt_ref4)based off appearance of line
spectrums:

sharp, principle, diffuse, fundamental





[\[e\]](#cmnt_ref5)follows alphabetical order





[\[f\]](#cmnt_ref6)lines = planes that create
nodes



the xy, yz, xz, etc. describes the intersecting
plane



the colors show +/- (color = +, white = -)





[\[g\]](#cmnt_ref7)normally\!





[\[h\]](#cmnt_ref8)actually increases with n





[\[i\]](#cmnt_ref9)not really\!





[\[j\]](#cmnt_ref10)remember this\!





[\[k\]](#cmnt_ref11)both are stable





[\[l\]](#cmnt_ref12)think about Koeval's
balloons\!





[\[m\]](#cmnt_ref13)nope





[\[n\]](#cmnt_ref14)most





[\[o\]](#cmnt_ref15)only for single step
reactions\!





[\[p\]](#cmnt_ref16)determined experimentally if
multi-step





[\[q\]](#cmnt_ref17)determined experimentally if
multi-step





[\[r\]](#cmnt_ref18)what it means to have high
temperature


