# Chapter 12: Acid-Base Chemistry

## Overview

* **Acid Physical Properties:** Sour, corrosive, react with bases.
* **Base Physical Properties:** Bitter, make your hand slippery (because it is kinda dissolving your hand!), react with acids.
* **Strong Acid:** ~100% dissolved in water. $$K_a >> 1$$
* **Weak Acid:** Less than 100% dissolved in water. $$K_a < 1$$
  * When considering strength of acid, you don't have to consider the molarity of water because it's constant in water.
* **Neutralization:** The reaction of an acid and base to form a salt.
* *N.B. You should always evaluate all reactions one electron at a time. The electrons aren't equal.*

### Arrhenius Theory

* **Arrhenius Acid:** Produces $H^+$ when immersed in _water_.
* **Arrhenius Base:** Produces $OH^-$ when immersed in _water_.
* _Weaknesses:_ Only considers things in water, implies all acids and bases act like ionic solids (_they don't!_)

### Bronsted-Lowry Theory

* **Bronsted Acid:** Proton ($H^+$) donor.
* **Bronsted Base:** Proton ($H^+$) Acceptor.
* _Weaknesses:_ Misses some things that still have acidic/basic properties.
* Encompasses Arrhenius Theory.

### Lewis Theory

* **Lewis Acid:** Electron Pair Acceptor.
* **Lewis Base:** Electron Pair Donor.
* _Weakness:_ Very broad definition of acids and bases.
* Encompasses Arrhenius Theory and Bronsted-Lowry theory.
* Strong acids have low unoccupied orbitals.
* Strong based have high occupied orbitals.

### Acid/Base Strength

* **Acid:** *Donates* proton easily. Have low energy unoccupied orbitals.
  * Electronegativity (of core atom) $\uparrow$ : Strength $\uparrow$. Because they are stealing/pulling on electrons from the hydrogen atom, making the hydrogen less tightly held.
* **Base:** *Takes* proton easily. Have high energy occupied orbitals.
  * Electronegativity (of core atom) $\downarrow$ : Strength $\uparrow$. Becuse they are taking positive charges (protons), not enegative charges (electrons).

## Section 12.1: Lewis Acids and Bases

* _BROAD_
* **Lewis Acid:** A substance with an empty orbital that can be used to form a bond by sharing a lone pair.
  * Must be able to accomodate additional electron pair. (Have less than 4 electron regions.)
  * Positive because it allow them to attack and bond to the lone pair. More positive makes it a stronger acid.
  * **Examples:** $AlCl_3$, $CO_2$, $SO_3^{2-}$, $Ag^+$, $H^+$
* **Lewis Base:** A substance that has a lone pair that it can share in a covalent bond.
  * Strength depends on electron density. AKA. What is being changed?
    * $CH_2O^− > HO^− > ClO^−$
  * **Examples:** $HS^-$, $C_2H_3O_2^-$
* **Lewis Acid-Base Reaction:** The conversion of the base's lone pair and the acid's empty orbital to a shared bonding pair.
  * Shown with a _curved arrow_.
* This bonding between acids and bases changes the molecule's hybridization and geometry! (Like all bonds do)

### Reaction Example

![Lewis acid-base reaction with curved arrows](media/12-acid-base-interactions/lewis-acid-base-reaction.png)
![Lewis acid-base reaction of silver and chloride](media/12-acid-base-interactions/silver-chloride-reaction.png)

### Comparing With Redox

* **Oxidizing Agent:** Receives electrons. Becomes reduced. Is already oxidized.
  * Strong oxidizing agents have low-energy unfilled orbitals.
* **Reducing Agent:** Gives electrons. Becomes oxidized. Is already reduced.
  * Strong reducing agents have high-energy filled orbitals.
* Reductant/Reducing Agent ~= Lewis Base
* Oxidant/Oxidizing Agent ~= Lewis Acid
* ***DIFFERENCE:*** Acid-Base interactions form covalent bonds / share electrons. Redox reactions form ionic bonds / transfer electrons.

## Section 12.2: Bronsted Reactions



### Class Notes

* **Coordinate Covalent Bond:** Bond formed by the reaction between two acids and bases.
* **Conjugate Acid-Base Pair:** The molecules that are formed when a molecule gains/loses a proton.

### Class Notes 12-3-18

* **Autoionization:** When water automatically ionizes itself with no input. E.g. $2H_2O \rightarrow H_3O^+ + OH^-$
  * Equilibrium Constant for Autoionization ($K_w$): $[H_3O^+][OH^-] = 1.00 * 10^{-14}$
* $pH =$ power of [H_3O^+] $= -log[H_3O^+]$
  * $pH \downarrow$ : $[H_3O^+] \uparrow$
* In neutral solution, $pOH = pH$
* **WHEN COUNTING SIG FIGS FOR LOGS, YOU ONLY COUNT THE NUMBERS AFTER THE DECIMAL PLACE.** This is because the digit after the log is simply the poewr of 10.
* **WHEN COUNTING SIG FIGS FOR NUMBERS TO POWER, YOU ONLY COUNT THE NUMBERS BEFORE THE DECIMAL PLACE.** This is because the digit after the log is simply the poewr of 10.

#### $K_a$
* $K_a$: The equilibrium constant for an acid's dissolution.
  * $HA(aq) + H_2O(l) \leftrightharpoons A(aq) + H_3O(aq)$
  * $K_a > 1$: Dissolution is favorable. Strong acids. Conjucate bases are insignificant.
  * $K_a < 1$: Dissolution is not favorable. Weak acids. Conjugate bases are significant.
  * Acid is weaker than water, it's *really* not an acid!
* $K_{rxn} = \frac{K_a(product-acid)}{K_a(reactant-acid)}$
* $K_{rxn} = K_a(product-acid) * K_a(reactant-base)$
  * Because $K_a$ and $K_b$ are reciprocals

### Class Notes 12-5-17

1. Write species in solution
  * Strong Acid: $A^- + H_3O^+$
  * Weak Acid: $A + H_2O$
  * Strong Base: $B^+ + OH^-$
  * Weak Base: $B + H_2O$
2. Eliminate all non-acids and non-bases
3. Write strongest acid and base as reactants
4. Write conjugate acid and base as products
5. Find $K_rxn = \frac{product-acid}{reactant-acid}$ to see if it is extensive or not
  * If $K_rxn > 1000$, extensive
  * Else, not

#### Acid-Base Table

* Always given as $K_a$ (equilibrium as acid)
* *Reciprocate to find $K_b$ (equilibrum as base)*
* If something is extensive as acid, it is a spectator ion as base.

* **Capture Arrow:** Shows what lone pair goes to make a bond with the proton.
* **Release Arrow:** Shows what bond goes to make a lone pair with the base.
