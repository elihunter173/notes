
# Chapter 9: Energetics

- Law of Conservation of
    Matter: matter can neither be created nor
    destroyed



- well, it can be turned into energy but
    shhh
- Means that chemical reactions must be
    balanced



- Energetics: thermodynamics
    + kinetics



- Thermodynamics: study of
    how energy moves and changes. only predicts whether or not reaction
    will occur



- consider difference in energy of products and
    reactants



- Kinetics: study of rates
    and mechanisms of reactions. predicts rate of reaction

## Section 9.1: First Law of Thermodynamics

- First Law of
    Thermodynamics: energy can neither be
    created nor destroyed.
- Thermal Equilibrium:the
    thermal point where all parts are at the same temperature and thus
    all energy is being transferred at equal rates

### Energy

- ![](media/imported/image47.png)



- ![](media/imported/image48.png): energy flows into the
    system.
- ![](media/imported/image49.png): energy flows out of the
    system.
- ![](media/imported/image50.png): no energy flow



- ![](media/imported/image51.png) always\! (1st
    law)

### Heat & Work

- q: heat absorbed by the
    system



- q \> 0: system absorbs
    heat.Endothermic
- q \< 0: system releases
    heat.Exothermic
- q = 0: system neither releases nor absorbs
    heat.Isothermic

![](media/imported/image161.png)                ![](media/imported/image156.png)

- w: work done by the
    system



- w \> 0: work is done by the system.
- w \



- ![](media/imported/image52.png)

## Section 9.2: Enthalpy

- Enthalpy (ΔH): the heat
    absorbed by a reaction when it is doneat a
    constant pressure.



- ΔH \> 0: heat absorbed
- ΔH \



- Standard States: What
    thermodynamic state a substance is in when its thermodynamic
    qualities are measured for standard calculations / table



- matters because thermodynamic qualities change
    for different states
- If something’s state isn’t stated, assume its
    standard state

### Thermochemical Reactions

- Standard Enthalpy of Reaction
    (ΔH°): enthalpy of reaction when all
    reactants are in their standard states
- Thermochemical
    Equation: chemical equation that takes
    into account the state and enthalpy of the reaction



- 2
    Na(s) + 2
    H2O(l)→ 2
    NaOH(aq) +
    H2(g)
     
     ΔH°
    = –368 kJ



- 368 kJ released from reaction of contents above
    when at their standard state



- you can do normal stoichiometry with the
    enthalpy\!

#### Properties

- the enthalpy of the reaction is proportional to
    the amount of moles
    reacted



- H2O(s)→ H2O(l)
     
     ΔH°
    = +6 kJ
- 2
    H2O(s)→ 2
    H2O(l)
     
     ΔH°
    = +12 kJ



- the enthalpy of a reaction is the opposite when
    the reaction is
    reversed



- H2O(s)→ H2O(l)
     
     ΔH°
    = +6
    kJ
- H2O(l)→ H2O(s)
     
     ΔH°
    = –6 kJ

## Section 9.3: Enthalpies of Combustion

- Combustion
    Reactions: reactions
    between material and
    oxygen



- Hydrocarbon Reaction:![](media/imported/image53.png)

### Properties of Combustion

- always
exothermic

![](media/imported/image174.png)

## Section 9.4: Bond Energies

### Estimating Enthalpies

- Using a table of bond types, enthalpy of any
    reaction can be estimated, just knowing what bonds formed and
    what bonds
    broke
- ![](media/imported/image54.png)



- Remember\! ΔH is energy absorbed



- Assumption:



- The C-Cl bond in
    CCl4 is the same
    as the C-Cl bond in
    COCl2 (it
    isn’t)



- The tabulated one is just the average



- All reactions are of gases (only enthalpies of
    gas bonds are calculated)



- This method works better for atoms which have
    few bonds of that type and are always gas

## Section 9.5: Entropy

- Systems
    firstlyminimize energy
    (H) and
    secondlymaximize entropy
    (S)



- ![](media/imported/image55.png)
- approximate using
    Sgas \> 0,
    Sliquid ≈ 0,
    Ssolid ≈
    0



- ![](media/imported/image56.png) produce gas
- ![](media/imported/image57.png) consume gas
- ![](media/imported/image58.png) neither produce nor
    consume gas



- Spontaneous
    Process:Process that occurs without
    intervention. Either reduces energy of system or increases
    entropy
- Entropy (S J/K): the
    numbers of ways energy can be organized in a system.
    statistical



- Things with more freedom of movement have higher
    entropy and things with less freedom of movement have lower
    entropy



- less freedom of movement tends to be
    ordered



- This is why entropy is commonly conflated
    with
    order



- Sgas \>\>
    Sliquid \>
    Ssolid
- ![](media/imported/image59.png)



- Think of temperature as preexisting order
- "Adding a book to the floor of a clean room
    matters a lot. Adding a book to the floor of a messy room doesn’t
    really cange much’



- High Entropy: think of as unconstrained

### Types of Motion

- Translation: straight
    line motion thru space
- Rotation:circular motion
    around axis (really along plane)
- Vibration: small
    perturbations about a central point
- Atoms have 3 degrees of freedom (can move in 3
    directions)



- Molecules have \# atoms * 3 degrees of
    freedom

## Section 9.6: Second Law of Thermodynamics

- ![](media/imported/image60.png) for all spontaneous
    reactions



- and all non-spontaneous reactions are powered by
    a spontaneous reaction

### Expanded

- ![](media/imported/image61.png)



- ![](media/imported/image62.png)



- ![](media/imported/image63.png)



- ![](media/imported/image64.png)



- ![](media/imported/image65.png)



- ![](media/imported/image66.png)

## Section 9.7: Gibbs Free Energy

- Gibbs Free Energy: the
    energy available for to a system



- ![](media/imported/image67.png)



- ![](media/imported/image68.png): spontaneous in
    reverse
- ![](media/imported/image69.png): spontaneous
    forwards
- ![](media/imported/image70.png): thermodynamic
    equilibrium



- ![](media/imported/image71.png):Enthalpically
    Driven



- Enthalpy favorable (exothermic) loses
    entropy
- Temperature ↓ : Reaction Rate ↑
- eg: water
    condensing



- ![](media/imported/image72.png):Entropically
    Driven



- Enthalpy favorable (exothermic) loses
    entropy
- Temperature ↑ : Reaction Rate ↑
- eg: water
condensing

![](media/imported/image181.png)

![](media/imported/image160.png)

### Units

- ![](media/imported/image73.png) kJ / mol
- ![](media/imported/image73.png) kJ / mol
- ![](media/imported/image73.png) J / (mol·K)

### Determining Entropy

- Look at moles of
gas

## Section 9.8: Standard Free Energy & Extent of Reaction

- Standard Free
    Energy![](media/imported/image74.png):

### Non-Standard States

- ![](media/imported/image74.png): Change in free energy
    atstandard state



- species in standard state (most stable form at 1
    atm)



- ![](media/imported/image73.png): Change in free energy
    atany state



- species at specific mixture, depends on reaction
    conditions



- Extensive:one side
    heavily favored



- the extensive side is the favored one

## Section 9.9: Kinetics

- Kinetics: Study of rates
    and mechanics in reaction
- Activation Energy
    (EA):the
    amount of energy needed to start reaction / reach transition state.
    think of it as a hump



- Origin: binding molecule
    as to bump off old molecule (transition state\!)
- Transition State: when
    the two molecules are both partially bonded. the highest energy
    (peak)



- Forextensive reaction:![](media/imported/image75.png)



- Ea is
    energy of activation
- This says that reaction is extensive if it takes
    less energy to start



- ![](media/imported/image76.png)

## Section 9.10: Reaction Rates

### Collision Theory Model

- Model and understand reactions by viewing them
    as atoms colliding and bonding because of that

#### Factors

- Concentrations: how many
    collisions
- Physical State: how many
    collisions and at what energy
- Temperature: how hard
    are the atoms colliding
- Orientation: do they
    have to be in a specific orientation? how easy is that?
- Catalysts: makes it
    easier to achieve correct orientation and decrease activation energy
    (weakens some bonds)



- enzymes\!
- Ozone Depletion: CFCs catalyze the breakdown of
    ozonex



- Collision Frequency:how
    many collisions occur per unit volume per second (mol ·
    L-1 ·
    s-1
- ![](media/imported/image77.png)



- For simple![](media/imported/image78.png)



- a is the rate order for A
- b is the rate order for B[\[o\]](#cmnt15)



- C depends on temperature, size of molecules,
    etc.



- determined experimentally\!



- Successful Collision
    Probability: X



- temperature dependent



- Rate Constant
    (k): probability that
    reaction -will occur given all the situations. basically, describes
    how easy it is to have the reaction



- Rate Constant ↑ : Ease of Reaction ↑ : Reaction
    Rate ↑
- depends on energy in system, difficulty of
    achieving correct orientation, etc
- how many steps are in the reaction
- Conceptually: rate
    rate/frequency is probability of collision forming product times the
    frequency of collisions



- Rate Law for Forward:![](media/imported/image79.png)



- a is rate order for A
- b is rate order for B[\[p\]](#cmnt16)



- Rate Law for
    Backward:![](media/imported/image80.png)



- x is rate order for
X[\[q\]](#cmnt17)

![](media/imported/image153.png)

### Determining Rate Order Experimentally

- ![](media/imported/image81.png)

### Notes

- it is really hard to
    figure out complex reactions using this theory



- do it experimentally\!

## Section 9.11: Equilibrium

- Equilibrium: When
    theforward rateand
    thebackwards
    rate are the same



- reactions
    aredynamic because
    they are constantly happening and adjusting to changes
    inboth directions



- Equilibrium
    Constant: ![](media/imported/image82.png)



- For ![](media/imported/image78.png)
- Represents the ratio of products to
    reactants
- DOES NOT GIVE ANY INSIGHT INTO
    RATE
- constant, irrespective of starting
    concentrations
- Ksp is the solubility constant. Just a
    special case of the equilibrium constant when its a solution\!



- What is the Rate Constant?



- For solids & liquids, not included



- because it doesn’t make sense to find molar density for solid.
    molar density is all that matters for
    equilibrium



- For gases, partial pressure
- For solutions, concentration
    (molarity)



- Catalysts:do NOT
    change equilibrium

### Derivation

- ![](media/imported/image83.png)
- Forward Reaction Rate: 
    ![](media/imported/image84.png)
- Reverse Reaction
Rate: 
    ![](media/imported/image85.png)
- Combined:
    ![](media/imported/image86.png)
    ![](media/imported/image87.png)
    ![](media/imported/image88.png)

![](media/imported/image188.png)

### Equilibrium Regions

![](media/imported/image190.png)

### Extensive Reactions

- To find how much of a product is produced, we
    normally assume that all of one of the reactants is used up



- only true for reactions with large K\! others
    have small amount of reactant left at equilibrium



- Large K → Extensive Reaction (almost only
    product)

### Thermodynamic Connection for Equilibrium

- ![](media/imported/image89.png) at equilibrium
- ![](media/imported/image90.png)



- ![](media/imported/image91.png) similar amounts
- ![](media/imported/image92.png) more product
    (extensive)
- ![](media/imported/image93.png) more reactant

#### Temperature

- When temperature is higher, the higher energy side of the reaction
    is favored because a larger % of the molecules are at a high
    energy[\[r\]](#cmnt18)\!

### ICE Tables for Finding Equilibrium

- I is initial
- C is change
- E is ending
- it’s like my pile method. think about it like
    finding thereaction
    moles

![](media/imported/image186.png)

## Section 9.12: Le Châtelier

- Le Châtelier’s
    Principle:when a reaction is stressed,
    it reacts in a way to minimize the stress



- Amount of Substance ↑ : Amount of Same Side ↓ :
    Amount of Other Side ↑



- think of it like balancing



- Removing solid or liquid doesn’t affect
    anything directly



- Exothermic:Treat heat
    as a product.
- Endothermic: Treat
    heat as a reactant.
