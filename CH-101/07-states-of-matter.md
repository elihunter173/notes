
# Chapter 7: States of Matter

- Solid: Low KE. Fixed
    Volume, Fixed Shape. molecules packed together closely. Low
    entropy
- Liquid: Medium KE. Fixed
    Volume, Dynamic Shape. molecules packed loosely together. Low
    entropy.
- Gas:High KE. Dynamic
    Volume, Dynamic Shape. molecules largely disconnected. High
    entropy
- Condensed States of
    Matter: Liquid and Solid
- Fluid States of
    Matter: Gas and
Liquid

![](media/imported/image124.png)

![](media/imported/image123.png)

- smaller molecules normally have higher boiling
    points than larger (stronger intermolecular attraction)
- polar molecules normally have higher boiling points
    than nonpolar (stronger intermolecular attraction)

## Section 7.1: Gases

take shape and volume of container, high energy,
basically independent particles

- Thermal Energy \

### Gas Qualities

- V: volume (L)
- T: temperature (K)
- n: amount (mol)
- P: pressure (atm, kPa, psi, mm Hg, torr,
    etc.)



- ![](media/imported/image30.png)
- 1 Pa = 1
    kg-m-1-s-2

### Laws of Gases

- Ideal Gas
    Law: ![](media/imported/image31.png)



- our powers combined\!



- Boyle’s Law: V ∝ 1/P
    (with constant n & T)
- Charles’ Law:V ∝ T (with
    constant P & n)



- charles is a Hotty McThotty
- origin of Kelvin (figured out that all gases
    have KE & V of 0 at absolute zero (\~-273.15 °C))



- Avogadro’s Law: V ∝ n
    (with constant P & T)



- Avogadro likes moles



- Gay-Lussac’s: P ∝ T
    (with constant V & n)



- pressure cooker\!

#### Derivations

- ![](media/imported/image32.png)
- ![](media/imported/image33.png)



- ![](media/imported/image34.png)

### Homogenous Mixtures

- Apply gas laws like other gas doesn’t
    exist



- pressure and volume of homogeneous mixture
    always equal



- Concentration:![](media/imported/image35.png)



- Molarity (type of
    concentration):![](media/imported/image36.png)



- Partial Pressure of Gas
    A:![](media/imported/image37.png)



- Partial Pressure = PART



- Homogeneous Gases can be considered hybrid
    gas



- Find moles from PV=nRT
- Use mass (from density and volume) and number of
    moles to find molar mass



- Total Pressure: sum of partial pressures
    (![](media/imported/image38.png))



- Dalton’s Law of Partial Pressures

### Stoichiometry

- Just like you’d expect.
- Do stoichiometry as you would normally. Then,
    figure out gas information from stoichiometry

### Measuring Tools

#### Pressure

- Barometer
- Manometer: used for computing the pressure of an
    unknown gas using atmospheric pressure. A U-shaped tube with the gas
    to be measured on one side and the atmosphere on the other. The
    pressure of the gas is determined by



- ![](media/imported/image39.png) (\`h\` is the difference
    in pressures as
height)

![](media/imported/image168.png)

## Section 7.2:Kinetic-Molecular Theory & Thermal Energy

- This theory deals with
    "Ideal
    Gases"
- Temperature: average
    kinetic energy of gas. also known
    asThermal Energy



- because not all particles moving at the same
    speed



- Thermal Energy: how much
    energy is required to keep up movement
- Interaction Energy: how
    strongly atoms are interacting



- explains why water isn’t a gas (hydrogen bonding
    interactions\!)



- Thermal Energy
    Approximation:![](media/imported/image40.png)



- from ideal gas law
- ![](media/imported/image41.png)z



- Heat:total kinetic
    energy of
gas

### Ideal GasAssumptions/Postulates

- gases consist of molecules in constant random
    motion
- KE of gas molecules depend solely on absolute
    temperature
- pressure arises from collisions with container
    walls
- no attractive or repulsive forces between
    molecules.
- volume of individual molecules is
    negligible[\[m\]](#cmnt13)

### Relationships

- All gases have same KE for a given T
- T ↑ : KE ↑ :
    Savg ↑
- Mass ↑ : KE – :
    Savg ↓

## Section 7.3: Intermolecular Forces

- Intramolecular
    Forces: bond forces holding molecule
    together. fixed based off structure
- Intermolecular
    Forces: forces between molecules and
    ions. fixed based off of structure
- these are completely disconnected\!
- Would a large molecular that bends over on
    itself and hydrogen bonds to itself be considered an intermolecular
    or intramolecular force?

### States of Matter

- Gas: overcomes
    attractive intermolecular forces
- Liquid: partially
    overcomes attractive intermolecular forces
- Solid: does not overcome
    attractive intermolecular
forces

### Types of Intermolecular Forces (IMF) (from Coulomb’s Law)

- Ion-Ion: very strong.
    from 2 ions (ionic bond)
- Ion-Dipole: fairly
    strong. from 1 polar covalent bond and 1 ion
- Dipole-Dipole Forces / Dipolar
    Forces: fairly weak. from 2 polar
    covalent bonds



- Hydrogen Bond: have a
    dipole-dipole interaction
    betweenH &N,
    O, or F 
- having a bond dipole doesn’t mean you have a
    molecular dipole



- only if molecule linear because they cancel
    out



- Induced Dipole: very
    weak, temporary dipole (induced dipole\!) arising from random
    electron movement



- causesdispersion
    forces\!
- brought about by nearby charges
- larger atoms have stronger dispersion
    forces (charges can move farther
    apart)
- more electrons means
    stronger dispersion force (more likely that there will be say 9
    extra on one side
- ratio of surface area to mass. Surface Area ↑ :
    Bonding Area ↑ :  Strength ↑



- think of velcro\!
- straight ones have dispersion force (are more
    flexible and can wrap around things)



- distorts electron cloud
- Determinants:Molecular
    Mass, shape (flatter/straighter is
stronger)

![](media/imported/image157.png)

![](media/imported/image141.png)

### Dipoles

- Center of Negative Charges = Pole
    (negative)
- Center of Positive Charges = Pole
    (positive)
- If they don’t coincide, the molecule has a
    dipole\!



- it’s a polar molecule
- can happen even if molecule is neutral as a
    whole



- Charge on Individual Atom: between Formal Charge
    and Oxidation State

#### Identifying Dipole

- Analyze the atomic charges and the the molecular
    geometry together
- ![](media/imported/image42.png)



- μ: dipole moment (kinda
    “strength of molecular magnet”
    or“distance between average
    charges”)
- q: magnitude of partial
    charge
- d: distance between
    opposite
poles

#### Representing Dipoles

![](media/imported/image149.png)

### Hydrogen Bonding

- Hydrogen Bond: have a
    dipole-dipole interaction between H & N, O, or F



- very strong\!
- because of all the atom’s small sizes and their
    extreme differences in electronegativity



- the two things that dipole-dipole strength
    relies on



- H must be directly bonding to N, O, or F and
    interacting with another N, O, or F



- Why is ice less dense than water? The more
    structured, crystalline intermolecular bonds have a higher average
    distances than the unstructured intermolecular
forces

![](media/imported/image162.png)

- why snowflakes are hexagonal fractals
- why water melts at high pressures (hollow spaces
    crack)

## Section 7.4: Solids

fixed position, low energy, definite volume

- Thermal Energy \> Intermolecular Forces
- we’ll read more later

## Section 7.5: Liquid

random motion, non-fixed but limited position, take
shape of container, have set volume

- Thermal Energy ≈ Intermolecular Forces
- they’re really complex :(

### Unique Properties of Liquids

- Cohesive Forces: forces
    between like molecules
- Adhesive Forces: forces
    between unlike molecules
- Viscosity: resistance /
    tendency towards flow.



- depends on energy of interaction compared to
    thermal energy



- Energy of Interaction ↓ → Viscosity ↑
- temporary bonds between molecules not readily
    broken



- also depends onshape of
    molecule (are they tangly)



- Surface Tension: the amount
    of force required to break thru the surface of a
    liquid.liquids try to minimize surface
    area. molecules on end have higher
    potential energy because fewer attracting interactions



- liquids are more attracted to themselves than
    the surrounding molecules
- soap decreases surface tension by getting in
    between the water molecules (it reacts more strongly with
    them)



- Meniscus: liquid is
    attracted to walls of container, so it forms a small film around the
    side called a meniscus



- due to adhesive forces \> cohesive forces
- opposite happens when cohesive forces \>
    adhesive
forces

![](media/imported/image143.png)

## Section 7.6: Phase Changes

- Phase Diagram: shows the
    phase of an element given a certain temperature and pressure



- along the line, both forms exist together



- Solid: strongest
    intermolecular forces (most stable, lowest energy)
- Liquid: medium strength
    intermolecular forces (medium stability, medium energy)
- Gas:weakest
    intermolecular forces (low stability, high energy)
- Gas \>\> Liquid \> Solid

### Heat Levels

- Heat of Fusion
    (![](media/imported/image43.png)): Solid
    → Liquid. Difference in energy of liquid and solid phase. Always
    positive
- Heat of Vaporization
    (![](media/imported/image44.png)): Liquid
    → Gas. Difference in energy of liquid and gas phase. Always positive
    and greater than heat of fusion
- Heat of Sublimation
    (![](media/imported/image45.png)): Solid
    → Gas. Difference in energy of solid and gas phase. Always positive
    and greatest



- ![](media/imported/image46.png)

### Temperature Levels

- Dynamic
    Equilibrium: Point where two rates of
    state changes occur at the same rate
- Melting
    Point: Temperature where Solid ↔ Liquid.
     Temperature where both solid and liquid exist



- High Melting Point → Strong IMF
- More Solid at High Pressure → Solid More
    Dense[\[n\]](#cmnt14)
- More Liquid at High Pressure → Liquid More
    Dense

![](media/imported/image177.png)                        ![](media/imported/image154.png)

Solid More Dense                Liquid More
Dense

- Evaporation/Vaporization:some
    gas will always escape because of naturally differing kinetic
    energies
- Condensation: some gas
    will lose enough energy to become liquid again
- liquids naturally reach equilibrium point of
    evaporation/vaporization and condensation
- Vapor Pressure:the
    pressure of the vapor (from evaporation) of a liquid at any
    temperature



- Temperature ↑ : Vapor Pressure ↑
- Strong IMF → Low Vapor Pressure



- few molecules can escape. likes being
    condensed



- Humidity = Observed % Water in Air / Vapor
    Pressure of Water at that Temperature
- Dew Point = temperature at which observed
    humidity in air is the theoretical vapor pressure of water



- humidity would hit 100% at dewpoint



- Boiling
    Point:Temperature where Gas ↔ Liquid.
    Point where the vapor pressure of a liquid equals the surrounding
    pressure



- bubbles with the given vapor pressure can form
    in liquid and not be crushed\!
- Normal Boiling
    Point:boiling point at 1 atm of
    pressure



- good indicator of IMF strength
- IMF Strength ↑ : Boiling Point ↑
- Mass ↑ : Boiling Point ↑

P-T Diagram / Phase
Diagram

![](media/imported/image176.png)

### Critical Point

- Critical
    Temperature: temperature where the
    density of liquid and gas are the same



- As you increase temperature of liquid-gas
    equilibrium, gas increases pressure and temperature
    decreases



- Critical
    Pressure:pressure where density of liquid
    and gas are the same
- Critical Point: critical
    temperature and pressure together
- Critical Density:density
    of substance at critical point
- Supercritical
    Fluid: “phase” when both temperature
    and pressure above critical point

### Phase Changes

- at phase changes, temperature does not
    change



- energy doesn’t go into increasing vibration
    rate / velocity, just breaking bonds.



- Heat Added Can: break bonds XOR increase
    temperature
- Boiling: Liquid → Gas
- Condensing: Gas → Liquid
- Freezing: Liquid → Solid
- Melting: Solid → Liquid
- Sublimation: Solid → Gas
- Deposition: Gas →
Solid

![](media/imported/image128.png)

![](media/imported/image148.png)
