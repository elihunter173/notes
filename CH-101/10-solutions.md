# Chapter 10: Solutions

## Vocabulary

### Mathematical Terms

* $k$: The rate constant. How fast the equation is going (forwards or backwards, depending on subscript).
  * Use $P_X$ (partial pressure of gas $X$) for gases.
  * $[X]$ (molarity of solute $X$) for solutions.
* $K$: The equilibrium constant. Determines where the equilibrium will be. ($\frac{k_f}{k_r}$).
* $K_{sp}$: The solubility constant. The product of the maximum solubilities and/or pressures
  > Just a special case of the equilibrium constant when its a solution
  * Can be used to find out how much precipitates out of solution when an amount is added.
  * If $K_{sp}$ is known and another molarity is known, the other can be found.

### Solution & Solvent Terms

* **Homogeneous Mixture:** A mixture with a constant composition and properties throughout. (Evenly mixed!)
* **Heterogeneous Mixture:** A mixture without a constant.
* **Solution:** A homogeneous mixture.
* **Solvent:** The part of the solvent that dictates the phase. "The Dissolver."
* **Solute:** Any part of solution that isn’t the solvent.
* **Concentration:** The ratio of solvents to solute.
* **Solvation:** The process of a solute going into solution.
* **Solvated:** After a solute has gone into solution.

* **Soluble:** $Solute \geq 0.1 M$
* **Slightly Soluble:** $Solute \geq 0.01 M$
* **Insoluble:** Anything else.
* **Miscible:** When two substances are soluble inside each other at all proportions.
  * e.g. alcohol and water
* **Immiscible:** When two substance are insoluble inside each other at all proportions.
  * e.g. oil and water
* **Saturated:** When solute concentration is maximized. (i.e. It has reached its solubility potential. No more can dissolve.)

### Water Terms

* **Hydration:** Solvation when the solvent is water.
* **Hydrated:** Solvation when the solvent is water.
* **Hydrophobic:** Doesn’t dissolve in water. (“Water” + “fearing/hating.”)
* **Hydrophilic:** Dissolves in water. (“Water” + “loving.”)

## Section 10.1: Concentration

* **Molarity:** Concentration in mol/L.
* To find the density of a solution, figure out the volume of it and ratio of solute to solvent.
  * $mass_{solution} \neq mass_{solvent}$

## Section 10.2: Solution Process

* Solubility is really just special extent/equilibrium reactions.
* To know if something is soluble, see if it is beneficial to dissolve.
  * Consider $\Delta G, \Delta S, \Delta H$
* $\Delta S$ is positive for dissolution, so just determine if $\Delta H$ is negative or slightly positive.
* Dissolving occurs when solute-solvent concentrations are strong.
* Like Dissolves Like (for polarity)

### Enthalpies

* $\Delta H_{solute}$: The energy required to separate solute particles. _Always positive._
* $\Delta H_{solvent}$: The energy required to create spaces between solvent particles. _Always positive._
* $\Delta H_{mixing}$: The energy change when the solute particles entre the cavities between the solvent particles. _Always negative._ (Hopefully enough such that the reaction occurs.)

$$\Delta H_{solvation} = \Delta H_{solute} + \Delta H_{solvent} + \Delta H_{mixing}$$

* **Beneficial:** ![Beneficial Enthalpy Equation](media/10-solutions/good-enthalpy.png)
* **Non-Beneficial:** ![Non-beneficial Enthalpy Equation](media/10-solutions/bad-enthalpy.png)

## Section 10.3: Organic Solutions

* **Organic Molecules** have hydrophobic (e.g. $C-C$ & $C-H$) and hydrophilic ($C-O$ & $C-N$) parts.
* To determine if substance is hydrophobic/hydrophilic, look at the relative sizes of their polar and non-polar parts.
* **Hydrophobic Parts** cause water to form ice-like structure around them.

![Alcohol Solubility Chart](media/10-solutions/alcohol-solubility.png)

## Section 10.4: Detergents & Micelles

* **Detergent:** A substance with a polar and a non-polar end, allowing non-polar substances (e.g. oil) to "dissolve" in polar substances (e.g. water) by actually dissolving in the detergent's non-polar end.
  * Have a "hydrophobic tail" and a "hydrophilic/polar head."
* **Soap:** A detergent with $COO^{1-}$ (carboxylate) group its polar head.
* **Monolayers:** Something that naturally forms when several detergent molecules are together. All the heads line up and form barrier against polar substance and all the tails line up and form barrier against non-polar substance.
* **Micelles:** When there are sufficient detergent molecules such that the monolayer curls up into a sphere. The hydrophobic tails are on the inside and hydrophilic heads are on the outside.
  * non-polar substances can dissolve inside the center of micelle!

![Micelle Illustration](media/10-solutions/micelle.png)

## Section 10.5: Electrolytes

### General Solubility Considerations

* **Coulomb’s Law:** Substances with a stronger attractive force when they're solid ions are less likely to dissolve. Solvents with stronger dielectric constant are more likely to dissolve things inside of them.
  * When you're happy as an ionic solid, there isn't much of a benefit for dissolving
  * When the solvent is highly shielding, it prevents the ions from feeling the effect of their ions,
    so they’re less likely to run into each other and become solid.
* **Higher Concentration:** When there is a higher concentration, it is harder to stay dissolved
  because you feel the effect of your ions more.

### Solubility Rules for Water

* **Ammonium Ions $(NH_4^+)$ & Group 1A Ions:** Generally soluble.
* **Nitrates $(NO_3^{2-})$:** Generally soluble.
* **Perchlorates $(ClO_4^-)$:** Generally soluble.
* **Acetates $(C_2H_3O_2^-)$:** Generally soluble. (Silver acetate only moderately so.)
* **Halides $(F, Cl, Br, ...)$:** Generally soluble.
  * _Except: $Ag^+, Pb^{2+}, Hg_2^{2+}, Tl^+$._
* **Sulfates $(SO_4^{2-})$:** Generally soluble.
  * _Except: $Sr^{2+}, Ca^{2+}, Ba^{2+}, Pb^{2+}$_
    > Sir Kabop!

### Misc.

* Isolated ions in equation are assumed to be aqueous.
* **Electrode:** A conductor where electricity enters/exits.
* **Ionic Compound:** Yields cation and anion when it dissolves.
* The acidic proton/$H^+$ is written first.
* **(Arrhenius) Acid:** Yields $H^+$ when it dissolves.
* **(Arrhenius) Base:** Yields $OH^-$ when it dissolves.
* **Strong Acids:** $HCl, HBr, HI, HNO_3, HClO_4, H_2SO_4$.
* **Strong Bases:** $OH^-$
* A strong acid/base overrides a weak acid/base
* **Electrolytes:** Ions that dissolve in water that help water conduct electricity. Either ionic compounds or acids.
* Electrolyte, Acid $\Longleftrightarrow$ Ionic Compound, Acid
* **Strong Electrolyte/Acid/Base:** Dissolves almost entirely in water
  * e.g. $HCl$, $CuCl_2$, $NaCl$, $KOH$, ionic compounds, metal hydroxides
* **Weak Electrolyte/Acid/Base:** Dissolves only partially in water. Most acids! Typically written with double
  arrows ($\rightleftharpoons$)
  * e.g. $CH_3CO_2H$ (acetic acid)
* **Non-electrolytes:** Molecules that dissolve in water but do not conduct electricity. They still have the $(aq)$
  because they are not ions.
  * e.g. sugar, ethanol, ethylene glycol
* **Supersaturation:** When something's concentration is greater than its theoretically maximum. This is very unusable
  because it is constantly trying to lose solute.
* _Dissolving is a dynamic process that is constantly changing and adjusting to external changes_
* **Spectator Ion:** Ions that have no effect on the single replacement reaction. Normally left out of equations.

## Section 10.6: Electrolyte Solutions

* The water dipole is strong so water has a strong _dielectric constant_ (aka. shielding).

## Section 10.7: Dissolution of Ionic Solids

* **Dissolution:** When something dissolves.
* To find out if something dissolves, consider Coulomb's law $F = \frac{k q*1 q_2}{\epsilon r^2}$. A substance dissolves when,
  * $r$ is large. This is affected by molarity.
  * $\epsilon$ is large. This is effected by the dielectric constant of the solvent. _The dielectric constant is essentially how well something shields charges._
  * $q_1 q_2$ is small. The charges in the ionic compound cannot be too strong because otherwise they would rather be together.
  * $F_{solute-solvent} >> F_{solute-solute}$

| Solvent Name         | Formula        | $\epsilon$ |
| -------------------- | -------------- | :--------: |
| acetic acid          | $CH_3COOH$     | 6.20       |
| benzene              | $C_6H_6$       | 2.28       |
| carbon tetrachloride | $CCl_4$        | 2.24       |
| ether                | $(C_2H_5)_2O$  | 4.27       |
| hexane               | $C_6H_{14}$    | 1.89       |
| water                | $H_2O$         | 80.10      |
| acetone              | $(CH_3)_2C=O$  | 21.0       |
| carbon disulfide     | $CS_2$         | 2.63       |
| dimethyl sulfoxide   | $(CH3)_2S=O$   | 47.2       |
| ethanol              | $C_2H_5OH$     | 25.3       |
| methanol             | $CH_3OH$       | 33.0       |

_N.B. Solubility is a spectrum._

## Section 10.8: Precipitation of Ionic Solids

* **Types of Equations:**
  * **Molecular:** Shows everything as if they were simply molecules interacting. Includes states of matter and charges.
  * **Ionic:** Shows everything but all aqueous solutions are dissociated, and ions are assumed to be aqueous.
  * **Net Ionic:** The same as ionic but only shows the parts of each side that change.
    * These cancelled/ignored/unchanged ions are called _spectator ions_.

## Section 10.9: Solubility Equilibriums

* When too much solute is added to a solvent, it can't all dissolve.
  It dissolves up to its solubility and then stops.
* $K_{sp}$: The solubility constant. It's exactly the same as the normal equilibrium constant except for dissolution.
