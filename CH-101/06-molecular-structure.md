
# Chapter 6: Molecular Structure

## Section 6.1: Molecular Shapes

- Valence Shell Electron Pair Repulsion
    (VSEPR): method of determining geometry of
    molecule. by minimizing the electronic repulsion by considering
    regions in high electron density
- General (Di)Atomic
    Form: AXmEn



- A is the central
    atom
- X is any atom/ligand
    surrounding A
- E a lone pair on A



- (High-Density) Electron Region / Steric Number
    (SN): any order bond, a lone
    pair.![](media/imported/image22.png)



- Coordination Number
    (CN):number of atoms around
    A.![](media/imported/image23.png)



- Bond Angle: the angle
    between two electron regions



- General Energy of Electron
    Regions: lone pair — lone pair \> lone
    pair — bonding pair \> bonding pair — bonding pair



- indicates angle (more energy = more space
    needed)



- COUNT THE NUMBER OF ELECTRON
REGIONS

### Ideal Bond Angle from Electron Region

![](media/imported/image129.png)[\[l\]](#cmnt12)

#### Deviations from Ideal

- Lone Pairs: take more space (attracted to atoms
    on side)



- more lone pairs take up even more space



- Higher Order Bonds: take more space



- consider resonance structures, otherwise VSEPR
    predicts wrongly



- Larger Atoms: take more space

### Molecular vs Electron Geometry

- Molecular
    Geometry: shape of molecule not
    considering lone pairs as atoms



- use this (almost) always\!



- Electron Geometry: shape
    of molecule considering lone pairs as atoms



- consider that lone pairs take more
space

![](media/imported/image134.png)



#### Examples



|                          |              |     |                                  |                                   |
| ------------------------ | ------------ | --- | -------------------------------- | --------------------------------- |
| Molecule                 | General Form | SN  | \[Electron Geometry\]{. c41 .c3} | \[Molecular Geometry\]{. c41 .c3} |
| BF3                      | AX3          | 3   | trigonal planar                  | trigonal planar                   |
| NO3\[-\]{.c 32 .c2 .c16} | AX3          | 3   | trigonal planar                  | trigonal planar                   |

### Molecular Representation (drawing\!)

- normally we use a line-wedge-dash representation
    to better show angles

#### Symbols

- Solid Line: bonds in plane of paper
- Solid Wedge: bonds extending before paper
- Dashed Wedge: bonds extending behind
    paper

#### Example

![](media/imported/image140.png)

#### Other Representations

![](media/imported/image125.png)

### Problems with VSEPR

- where the fuck is the consideration
    fororbital
    shape?
- how can we understand the shape of bonds?

## Section 6.2: Expanded Octets

- Only C, N, O, F, follow octet rule
    strictly



- others can have expanded octets because of energy
    similarity ofd-block
- Example: SF6
- well, oxygen can easily have -1 FC (1 too many
    bonds)



- How do you tell? when,
    there’s not enough shared pairs predicted from ER to accommodate the
    number of bonds



- ER is expecting an octet, so if it’s expanded
    then no worky\!



- ![](media/imported/image24.png)



- only works on central atom



### Defined Shapes

- Trigonal Bipyramidal: 5 electron regions

## Section 6.3: Larger Molecules

- look at each individual atom and its bonds to
    predict bond lengths and bond
angles

## Section 6.4: Valence Bond Theory & Hybridization

- Valence Bond Theory: the
    use of geometric algebra to understand molecular/electron shapes.
    actually makes sense unlike the memorization of VSEPR

### Types of Bonds in VB Theory

- all covalent bonds are the
    overlapping/hybridization of orbitals. therefore, given different
    orbitals being overlapped, there are different types of covalent
    bonds
- the bonds don’t have to be to like
    orbitals
- Single Bonds:1 σ
- Double Bonds:1 σ, 1
    π



- there’s only one s orbital\!



- Triple Bonds:1 σ, 2
    π

#### Sigma Bonds (σ)

- The basic, fundamental, necessary type of
    bond
- Head-on-Head overlap of s or p orbitals with
    only one electron in them (with opposite spin)
- cylindrical symmetry of electron density on
    internuclear axis
- electron density concentrated b/w two nucleuses
    (along internuclear
axis)

![](media/imported/image163.png)

#### Pi Bonds (π)

- Can only exist because sigma bonds pull
    orbitals close enough to be side-on-side bonds
- Side-on-Side overlap of
    p or d orbitals with only one electron in them (with opposite
    spin)
- only exists when sigma bond exists



- unpaired orbitals that just happen to bind
    together when sigma bonds form



- makes bond stronger
- no electron density on internuclear axis
- prevent rotation



- fixed by “2 bars” on the side
- spinning would break the π
bond

![](media/imported/image183.png)

#### Delta Bonds (δ)

- only from bonding d orbitals

### Hybridization

- sigma, pi, and delta bonds don’t work in
    non-diatomic molecules
- Hybridization: the
    overlapping of electron
    orbitals.the summing of
    3d wave functions (do twice, swapping the phase on one the second
    time) and then average those two summed orbitals.



- why do both phases



- Hybrid
    Orbitals: overlapped orbitals.



- can sum arbitrarily large orbitals
- named: all the orbitals that went into making it
    (without n)



- ex: sp,
    sp2,
    sp3



- explains why atoms bonding to 3 atoms can only
    form one double done\! they only have one extra non-hybridized
    orbital floating
around

![](media/imported/image151.png)

#### Rules of Hybridization

- all atom’s bonds use same hybridized
    orbital.number of orbitals to hybridize
    is the number of bonding electron regions
- number of hybrid orbitals = number of orbitals
    that went in to make
- hybridized orbitals always form sigma
    bonds
- always hybridizelowest
    energy orbitals first
- don’t have to be same hybridized orbitals within
    or across atoms
- each electron region has own its hybrid
    orbital



- but all atom’s bonds use same hybridized orbital
    (separate but equal)



- unused orbitals can form double and triple
    bonds\!

#### Rules Illustrated

![](media/imported/image136.png)![](media/imported/image147.png)

- C uses sp2
- O uses sp2
- H uses s
- TADA\! look how the double bond forms\!
    it’s just lower energy :)



- also\! this is why it’s smaller\!
:D

## Section 6.5:Molecular Orbital Theory

- view on a per-bond basis using all connected atomic
    orbitals.delocalized from
    atoms



- views all similar bonds as the same, detached from
    individual atoms\!

### Main Principles of MO Theory

- Number of Molecular Orbitals = Number of
    Constituent Atomic Orbitals
- Bonding Molecular Orbitals
    (MOs): in-phase atomic orbitals (AOs).
    decreased energy from constituent orbitals. increased electron
    density between



- increased electron density between
    nuceluses
- ex: σ, π



- Anti-Bonding Molecular Orbitals
    (MOs): out-of-phase atomic orbitals
    (AOs). increased energy from constituent orbitals. decreased
    electron density between



- decreased electron density between
    nuceluses
- ex: σ*, π*



- MOs easier when electrons have similar energy
    because they contribute more equally so they can be
    approximated
- Molecular Orbitals successfully accounts for
    lone electrons and paramagnetism\!

### Creating MO Diagrams.

- Creating MO
    Diagram: combine
    like oritals, with bonding being lower energy than antibonding.
    otherwise, it is like a normal orbital energy chart



- for O & F,
    σ2p \<
    π2p



- because the paired 2p orbitals
    makeπ2p higher
    energy(not really, it’s actually essentially
    complex math)



- Filling MO
    Diagram: construct MO diagram and then
    fill in using Hund’s Rule (same spin) and Pauli Exclusion Principle
    (no identical electrons)



- use diagram to determine paramagnetism (lone
    electrons) or dimagnetism (all
full)

![](media/imported/image171.png)

#### Diagrams

![](media/imported/image146.png)

MO Diagram of
NO-

![](media/imported/image25.png)



![](media/imported/image139.png)

H MO
diagram



![](media/imported/image119.png)

He MO Diagram

### Energy

- ![](media/imported/image26.png) is how much
    thebonding interactions
    decreases the energy of the
    system.
- ![](media/imported/image27.png) is how much
    theantibonding interactions
    increase the energy of the
    system.



- ![](media/imported/image28.png)



- HOMO: HighestOccupiedMolecularOrbital
- LUMO: LowestUnoccupiedMolecularOrbital



- higher than HOMO



- difference between HOMO and LUMO determines many
    properties of molecules

#### Heteronuclear Diatomic Molecules

- if 2 atoms are different, their contributions to
    the molecular orbitals will be of different energies
- just consider each one of them separately when
    filling MO



- fill lowest energy MO first always\!



- lower energy contributes more (has things bond
    to it first)
- different rates of filling explains polarity
    and dipoles\!



- larger energy difference means more polar
- lower energy one has higher electronegativity
    because it’s MOs are being filled first

### Determining Bonds

- ![](media/imported/image29.png)
- use this to determine if bond would actually
    form or not\!



- so this is the number of bonding and antibonding
    electrons if this bond occured, what would the order be?

### Bonding, Antibonding, and Nonbonding

- Number MOs = Number constituent AOs
- More Nodal Planes = Higher Energy
- Nodal Planes Symmetric
- Nodal Planes not on Adjacent Atoms



- doesn’t make sense. can’t split



- Nodal Planes not on Terminal Atoms



- doesn’t make sense. can’t split



- use these rules to build up from no nodal
    planes to all nodal planes for complex
molecules\!

![](media/imported/image185.png)

#### Determining by Math

- you can do this for any bond, whether it exists
    or not\!
- B is Bonding Electrons
- A is Antibonding Electrons
- B \> A is bonding



- lower energy than AO



- B \



- higher energy than AO



- B = A is nonbonding



- equal in energy to AO

### Magnetism

- Diamagnetic: only paired
    electrons



- doesn’t react to magnetic field because they
    can’t align (cancel out)



- Paramagnetic:has some
    unpaired electrons



- reacts to magnetic field because unpaired
    electrons can react to external electric fields
