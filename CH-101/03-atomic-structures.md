# Chapter 3: Atomic Structures

## Section 3.1: Valence Electrons

- Core Electrons: electrons in the filled
    inner-shells; unaffected by chemical reactions
- Valence Electrons: electrons in the outermost s
    sublevel or unfilled inner-shells; affected by chemical
    reactions



- explains how elements bind and
    react[\[i\]](#cmnt9)
- Nvalence
    electrons = group \#

### Oddities

- Cu: even tho
    4s1 3d10,
    its valence electrons are
    4s1 3d10 (full
    3d shell still involved with bonding)
- Noble gases don’t have valence electrons (all
    shells
inner-shells)

## Section 3.2: Shielding and Effective Nuclear Charge

- Effective Nuclear Charge
    (Zeff): the
    charge that
    isreallyfelt by
    an electron after taking into account the shielding due to
    inner-electrons. by default calculated as the outermost
    electron



- σ: shielding constant (never perfect (ie:
    1))



- σcore \>
    σouter



- we don’t calculate here\! only estimate
    relatively\!
- ![](media/imported/image7.png)



- Effective Nuclear Charge determines almost all
    other atomic
trends

### Zeff Trends

- ↑ downwards (group)



- because shielding is less effective farther
    away



- ↑ rightwards
    (period)



- because shielding is less effective in valence
    shell



- Metals: low Zeff
- Nonmetals: high
    Zeff

![](media/imported/image127.png)

## Section 3.3: Relative Atomic Size

- atomic radius kinda arbitrary because atoms
    aren’t hard spheres



- why we use relative\!

### Using the Bohr Model for Understanding

- Bohr model is partially incorrect, but can be
    used to understand trends
- ![](media/imported/image8.png)

### Trends

- larger on
    theleft,bigger
    on
    thebottom
- Firstly: ↑ downwards (group)



- because
    n2 grows
    fast



- Secondly: ↑ rightwards (period)



- because
    Zeff grows
    slower

## Section 3.4: Relative Orbital Energies

- Orbital Energy: the
    energy required to keep an electron bound



- Orbital Energy ↑ : Stability ↓
- also think of it as closer to 0 is less
    stable
- IMPORTANT: equation
    isNEGATIVE



- equal and opposite to ionization energy

### Using the Bohr Model for Understanding

- Bohr model is partially incorrect, but can be
    used to understand trends
- ![](media/imported/image9.png)



- X: element
- n: principle quantum number of valence
    shell

### Trends

- larger on
    theleft,bigger
    on
    thebottom
- ↑ downwards (group)
- ↑ rightwards
    (period)
- Nonmetals: low energy
- Metals: high energy

## Section 3.5: Ionization Energy

- Ionization
    Energy: how much energy is required to
    remove a valence electron (ie: how much energy it takes to
    ionize)



- ![](media/imported/image10.png)



- because
    Efree =
    0



- X →
    X+ +
    e- ΔE=IE

![](media/imported/image184.png)

- equal and opposite to orbital energy

### Trends

- ↓ leftwards (period)
- ↓ downwards (group)
- “surprisingly” high when removing electrons
    fromfilled/half-filled[\[j\]](#cmnt10) shells
- Metals: low IE
- Nonmetals: high IE

## Section 3.6: Electronegativity

- Electronegativity (χ): strength with which atom
    attracts bonding/free electrons



- lower energy / closer valence orbitals are more
    attractive because things like low energy



- ![](media/imported/image9.png)



- low energy = high electronegativity

### Trends

- ↑ upwards (group)
- ↑ rightwards (period)
- Metals: low electronegativity



- Late Metals:high
    electronegativity (d & f shells shield
    poorly because of number of nodal planes)



- Nonmetals: high electronegativity

### Bonding

- Covalent Bonds: bonds where the electron is
    shared



- the more electronegative element has the electron
    more of the time and receives a partial negative charge
    (-δ)



- Ionic Bonds: bonds where an electron is
    given

## Summary of Atomic Trends

### Summary of Values

- Zeff:
    how much (positive) charge is felt by outermost electron
    /how strongly electron
    pulled
- rn:
    how large the atom is /how tightly
    the atom is
    held
- En:
    how much energy is required to keep electron there or how much
    energy electron has /how
    happy/relaxed the electron is
- IE: how easy it is to remove outermost electron
    /how much the electron wants to
    leave
- EN: how strongly the atom pulls other electrons or
    what is the energy of the lowest level empty orbital
    /how much the atom wants another
    electron or how hungry the atom
    is

### Summary of Relations

- Zeff↑
    : rn ↓ :
    En ↓ : IE ↑ :
    EN ↑
- ne
     ↑ :
    rn ↑ :
    En ↑ : IE ↓ :
    EN
    ↓

### Summary of Extremes

- Zeff:
    highest = top right, lowest = bottom
    left
- rn:
    highest =
    bottomleft, lowest =
    top
    right
- En:
    highest = bottom left, lowest = top right
- IE: highest = top right, lowest = bottom
    left
- EN: highest = top right (excluding noble gases),
    lowest = bottom left (and noble gases)

# Ionic vs Covalent Bonds

## Ionic

- nonmetals + metals
- electrons transferred
- high![](media/imported/image11.png)
- bonds non-directional



- all bond distances equal in all direction, given
    to same parts of atoms



- “ionic” substances/network (crystals)
    formed
- atoms not bound to particular atom, just loosely
    surrounded by opposite charges due to loss/addition of
    electrons

## Covalent

- nonmetals + nonmetals
- electrons shared
- low![](media/imported/image11.png)
- bonds directional
- molecules formed
- atoms are bound to particular atom

