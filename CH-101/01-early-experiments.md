# Chapter 1: Early Experiments

## Section 1.1: The Scientific Method

### Steps to the Scientific Method

1.  Observe



2.  Quantitative Observation: number;
    quantity
3.  Qualitative Observation: adjectives;
    quality



4.  Hypothesize: form explanation/hypothesis
5.  Predict: use hypothesis to predict
    results
6.  Test/Experiment: see if your prediction was
    accurate

### Terms

  - Theory: accepted explanation of
    observation
  - Law: constant observation (NO
    EXPLANATION)

### Historical Theories

  - Phlogiston: all burnable materials have
    “phlogiston” which is removed thru burning



  - calx of X: “dephloginated” X

## Section 1.2: Lavoisier

### Lavoisier’s Laws

  - Conservation of Mass: mass never changes in
    chemical reaction
  - Definite Proportions: compound is always made up
    of same percentage of elements by weight, no matter amount
  - Multiple Proportions: 2+ compounds made up of
    the same materials will always have a mass ratio of small whole
    numbers when comparing the same element

### Lavoisier’s Theories

  - Element: pure substance that can’t be broken
    down by chemical means
  - Compound: pure substance made up of multiple
    elements

## Section 1.3:Dalton’s Atomic Theory[\[a\]](#cmnt1)

### Main Components of Theory

  - elements composed of atoms. atoms of same
    element have identical chemical properties
  - atoms are not changed themselves in chemical
    reactions
  - compounds are combinations of atoms of different
    elements. the number of atoms in a compound is integral and
    constant

## Section 1.4: Atoms and Molecules

### Terms

  - Atom: smallest unit element can be in, where it
    still has its properties
  - Molecule: smallest unit compound can be in,
    where it still has its properties
  - Element: substance with one type of atom
  - Compound: substance with two + types of
    atom

### Laws

  - Avogadro’s
    Law: equal volumes of gases at same
    temperature and pressure have the same number of molecules



  - (Earlier) Law of
    Combining Volumes: volumes of reacting gases at same temperature and
    pressure always in small whole number ratio

### Rules

  - Balancing Equations: must have same number of
    each type of atom on both sides of chemical equation



  - from Conservation of Mass
  - Method: start with largest molecule and set
    coefficient to one, cascade. if not work; increment largest
    molecule, cascade



  - coefficients can be edited to balance
  - subscripts can’t be edited to balance

## Section 1.5: The Mole and Molar Mass

  - Atomic Mass (A) (amu): how much mass a single
    atom has



  - H = 1 amu
  - ≈
    p+ +
    n0



  - Molecular Mass (amu): how much mass a single
    molecule has



  - =
    n1 *
    A1 +
    n2 *
    A2 + …



  - Avogadro’s
    Number: the conversion ratio of grams :
    amu



  - = 6.022 * 1023


  - Mole (mol):a
    Avogadro’s Number of things
  - Molar Mass: how many grams are in a mole of
    something (same as Atomic Mass or Molecular Mass but in
    grams)
  - Dimensional Analysis / Factor Label Method: how
    to convert units, treating them as if they are
coefficients

![](media/imported/image155.png)

## Section 1.6: Stoichiometry

  - Stoichiometry: study of the conversion of one
    substance to another chemically equivalent
amount

![](media/imported/image167.png)

![](media/imported/image189.png)

  - Limiting Reactant: one reactant is lacking and
    determines how much product can be produced. using stoichiometry and
    arithmetic you can find how much product is produced and how much of
    other reactants is left over from knowing limiting reactant



  - Find Limiting Reactant



  - see how much product each individual reactant
    can produce if others in excess
  - determine smallest amount
  - that one is the limiting reactant and how much
    is actually produced



  - Finding Remaining Reactants



  - see how much of limited product is made
  - use that to determine how much of excess
    reactant was used
  - subtract used amount from available
    amount
  - that is the remaining amount

## Section 1.7: Energy

  - Potential Energy (U): energy of position
  - Kinetic Energy (KE): energy due to
    motion[\[b\]](#cmnt2)



  - Thermal Energy (jiggly atoms)
  - Kinetic Energy



  - Law: energy can be
    neither created nor destroyed, only changed
  - SYSTEMS ALWAYS SEEK LOWEST ENERGY LEVEL



  - most stable configuration



  - ΔE:
    Ef -
    Ei



  - ΔE \
  - ΔE \> 0 → gained
energy

## Section 1.8: Electromagnetism and Coulomb’s Law

  - Charles Coulomb first to measure electromagnetic
    force
  - Coulomb’s Law in functions

## Section 1.9: Early Atomic Models

  - Dimitri Mendeleev: discovered Periodic Law and
    created Periodic Table

### Atomic Models

  - Kelvin-Thomson Plum-Pudding Model: positive
    charge diffuse, surrounding electrons
  - Nuclear Model: mostly empty space; nucleus of
    p+ and
    n0 in center
    orbited by e-

### Experiments

  - Thomson Cathode Ray Experiment: see if cathode
    ray is
    deflected



  - e- exist
    and make up cathode ray
  - charge-mass ratio



  - Milikan Oil Drop Experiment: find mass of droplet
    by terminal velocity, stick
    e-’s to it
    and find charge of oil drop by how much charge suspends drop



  - Method:![](media/imported/image1.png)
  - ![](media/imported/image2.png)
  - ![](media/imported/image3.png) (from Thomson’s
    charge-mass ratio)



  - Rutherford Gold Foil Experiment: shoot α particles
    (He2+) thru
    gold foil to see how they deflect



  - Kelvin-Thomson Plum-Pudding Model: almost all
    particles not deflected, some very slightly
  - Actual: yes, but some particles acutely
    deflected
  - New Model: Nuclear Model of
Atom

## Section 1.10: Subatomic Particles, Isotopes, Ions



| Particle Name/Type | Mass (amu) | Charge | Location       |
| ------------------ | :--------: | :----: | -------------- |
| proton (p+)        | 1.0073     | 1+     | nucleus        |
| neutron (n0)       | 1.0087     | 0      | nucleus        |
| electron (e-)      | 0.0005     | 1-     | electron cloud |

  - Atomic Number (Z) = \# of
    p+
  - Mass Number (A) ≈
    p+ +
    n0
  - Charge (q) =
    p+ -
    e- (\#
    -/+)
  - Elemental Symbol (X): 1-3 letters representing
    element
  - Written![](media/imported/image4.png)
  - Isotopes: same element, different mass (same Z,
    different A)



  - Ion: same element, different charge (same Z,
    different C)



  - Anion: negative
    ion
  - Cation[\[c\]](#cmnt3):
    positive ion

## Section 1.11: The Periodic Law

  - Periodic Law: elements in same group have
    similar properties



  - 1869 Dimitri Mendeleev discovered



  - Period: row
  - Group: column
  - Atomic Mass: weighted average of isotopes as
    they occur on
earth

![](media/imported/image170.png)

### Named Groupings of Elements

  - Main Group Elements: groups 1-2 and 13-18 (the
    ’a’s)
  - Transition Metals: groups 3-12 (the ’b’s)
  - Metals: ductile, lustrous, malleable, conduct
    heat, conduct electricity



  - Alkali Metals: react strongly (with
    water)
  - Alkaline Earth Metals: react weakly with
    water
  - Transition Metals: “normal” metals
  - Lanthanides: first period of bottom area
  - Actinides: second period of bottom area



  - Nonmetals: opposite of metals



  - Halogens: form salts, react strongly
  - Noble Gases: extremely stable and
    non-reactive



  - Metalloids: kinda metal, kinda
nonmetal

