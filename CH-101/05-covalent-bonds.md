
# Chapter 5: Covalent Bonds

## Section 5.1: The Covalent Bond

- Covalent Bond: sharing of
    electrons between 2+ atoms / overlap of orbitals. because of similar
    electronegativities
- Bond Length: how far
    apart the nucleuses of 2 atoms are at when at their minimum
    energy



- normally 100-300 nm

### Forces of Covalent Bonds

- Electron
    Attraction: electrons attracted to
    other’s nucleus
- Nuclear Repulsion: one’s
    nucleus repels
others

![](media/imported/image152.png)

- Bond Energy: the
    difference between the energy of the 2 atom system when bound and
    when non-interacting. the amount of energy “saved” by being bound.
    the amount of energy required to break bond



- always negative (bonds are low energy)

## Section 5.2: Bond Polarity

- Bond Polarity: when two
    atoms with different electronegativities are bound, their bond is
    polar due to unequal sharing



- the more electronegative (“greedy”) one pulls
    electrons more strongly, so they spend most of their time around
    that atom



- Bond Dipole:when 1+ side
    (pole) of atom has slight charge (shown δ)



- moreelectronegative =
    δ-
- lesselectronegative =
    δ+

### Bonding Polarity Spectrum

- ![](media/imported/image12.png) (![](media/imported/image13.png))
    is a measure of the change in charge from dipole to dipole
- Purely Covalent: perfect sharing, no
    charge.![](media/imported/image14.png)



- electronegativities same



- Polar Covalent: unequal sharing, has (slight)
    charge or dipole.![](media/imported/image15.png)



- electronegativities similar



- Ionic Bonds: complete transfer, both have extreme
    and opposite charges.![](media/imported/image15.png)



- electronegativities very different



- look and base covalent/ionic based off of this
    number, not the general metal-nonmetal or nonmetal-nonmetal
    rule

#### Polar Covalent Bonding

![](media/imported/image179.png)

![](media/imported/image121.png)![](media/imported/image130.png)![](media/imported/image137.png)



## Section 5.3: Naming Binary Covalent Compounds

- Binary
    Compound: compound of only 2
    elements
- “Greek \# prefix + element” + “Greek \# prefix +
    element + ‘ide’”



- Element
    Order: increasing electronegativities,
    except acidic hydrogens go first



- Acidic Hydrogen: hydrogen that makes something
    an acid



- If “Greek \# prefix” is “mono”, you can leave it
    off

### Greek Prefixes

![](media/imported/image158.png)

## Section 5.4:Lewis Symbols

- Lewis Symbol: shows the
    distribution of valence electrons within atom



- electrons shown in 4 possible regions, follows
    Hund’s rule



- For main group elements, \# valence electrons =
    main group
number

#### Chart

![](media/imported/image169.png)

## Section 5.5: Lewis Structures of Diatomic Molecules

- Lewis Structure: shows the
    distribution of valence electrons ina
    molecule andatom. multiple Lewis symbols
    grouped together and bonded



- simple and good at predicting molecular shape,
    types and strengths of bonds, and regions of high/low electron
    density\!
- but imperfect and hand-wavy



- Octet Rule: try to get
    atoms to 8 total electrons in valence shell.



- hydrogen and helium to
2

#### Terminology Diagram

![](media/imported/image165.png)

### Valence Electrons

- electrons in outermost n level and unfilled
    inner levels
- group \# = \# of valence electrons
- Shared (bond
    pairs) or Unshared
    (lone
    pairs)

### Bond Order

- Bond Order: a measure of
    the strength of a bond. determined per bond
- Single Bond: 2 electrons. order = 1
- Double Bond: 4 electrons. order = 2
- Triple Bond: 6 electrons. order = 3
- all atoms can form up to 4 bonds



- Boron can only form 3 (3 valence electrons to
    share)
- Hydrogen can only form 1 (1 valence electron to
    share)



- electrons involved in bond ↑ : strength of bond
    ↑ : length of bond ↓

## Section 5.6: Determining Lewis Structures

### General Method

1.  find valence electrons
2.  find central atom



1.  high electronegativity and forms many bonds
    (carbon\!)



3.  place at least 1 bond between bonded
    atoms
4.  fill in remaining lone electrons on terminal
    atom (only bonded to 1)
5.  make more central atom “happy” by adding
    bonds



1.  repeat until everything is good

#### Calculations

- use these to assure that your final diagram is
    right
- Variables:



- SP is the number of
    shared pairs
- LP is the number of lone
    pairs
- ERis the total number of
    electrons required to individually satisfy each atom in the
    molecule
- VEis the total number of
    valence electrons of the atoms in the molecule



- Functions:



- ![](media/imported/image16.png)
- ![](media/imported/image17.png)

### Alternatives to Lewis Structure

- Hybridization/Molecular Orbital Theory



- predicts electronic properties very well
- complex :(

## Section 5.7: Resonance

- Resonance: the
    ability/property of an electron where it can change what atoms it is
    bonding.electrons can move around within
    molecule
- Resonance Structure:Lewis
    structure showing the different equally possible configurations of
    electrons in a molecule.the permutations
    of electron bonding



- curved arrows show the “movement” of
    electrons
- occasionally condensed into one structure with
    dotted bonds for possible
bonds

![](media/imported/image172.png)

- Not all molecules
    haveresonance structures



- mostoxoanions do

### When do Resonance Structures Appear?

- when SP \> identical bonding regions



- when there are identical bonding regions that
    can have multiple possibilities



- the number of possible permutations is the
    number of resonance structures

#### Determining Bond Order

- ![](media/imported/image18.png)



- for identical bonding
regions

## Section 5.8: Formal Charge & Oxidation State

- ![](media/imported/image19.png)



- VEfree is
    how many valence electrons that atom would have if unbonded and
    neutral
- NB is non-bonding electrons (its
    electrons)
- BE is bonding electrons (shared
    electrons)



- a is how the electrons are shared
- Formal Charge (FC): the
    (apparent) charge of atom in a molecule assuming all electrons
    shared equally



- a = 1/2
- ∴![](media/imported/image20.png)



- Oxidation State
    (OX): the (apparent) charge of atom in a
    molecule assuming all electrons transferred (ionic)



- a  = 1 or 0 (or 1/2)



- more electronegative gets +1



- gets electrons



- less electronegative gets 0



- doesn’t get electrons



- equal electronegativity gets 1/2



- share electrons equally



- both sum to zero\!

### Ideal States

- FC = 0



- minimize![](media/imported/image21.png)



- most electronegative atom has lowest FC

### Uses of Formal Charge

- shows dipoles



- Dipole: element that has
    a slight charge on one side



- Optimizing Lewis Structures: keep molecule at/near
    ideal states
