# Conventions

## Naming

- Classes in PascalCase or UpperCamelCase
- Methods in camelCase or lowerCamelCase
- Variables in camelCase or lowerCamelCase

## Javadoc Comments

- **Javadoc:** A utility that creates documentation from comments within your code. IDEs can also utilize it for tooltips.
- Javadoc uses various tags to identify parts of code. Utilities such as checkstyle and many IDEs will tell inform you if you are missing Javadoc comments. Additionally, many IDEs will create skeleton Javadoc for you to fill in.
- *What to Javadoc?* Classes, Methods, Constants, anything ambiguous.

### Javadoc Tags

- **`@param parameterName`:** Identifies and describes the parameters of a method.
- **`@returns`:** Describes what the method returns.
- **`@throws ExceptionName`:** Describes what exceptions a method may throw and under what circumstances.

#### Optional Tags

- *These tags provide additional information for your comments but are not strictly necessary.*
- **`{@value ClassName.CONSTANT}`:** Displays the value of the constant.

### Syntax

#### Inside Code

```java
/**
 * Comments go here.
 */
```

#### Compiling Javadoc

```sh
javadoc filename.java
```
