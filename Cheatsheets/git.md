# Git

## Terminology

- **Repository (Repo):** A directory that contains all the files for a project and a database including each files version and change history.
  - **Local Repo:** A repository stored on a local machine. *This is part of what makes git unique!*
  - **Remote Repo:** A repository stored on a common, shared machine. *What allows the sharing!*
- **Staging Area / Index:** A temporary database containing all the files that have changed but not how.
- **Commit:** A snapshot stored in the git database that describes everything that changed at that time.
- **Revision:** A specific commit or collection of commits.
- **Branch:** The repo with a specific commit history. You can think of it as a version of the project.

## Commands

```shell
git status # Displays the status of the git repository
git log # Shows the log of recent comments

### Most Common ###
git clone URL # Clones a remote repo to your local machine
git add filename # Adds files to the staging area
git commit # Commits files to the local repo
git push # Pushes commits from the local repo to the remote repo
git pull # Pull commits from the remote repo

### Branches ###
git branch branchName # Creates a new branch
'' -d '' # Deletes a branch
git checkout branchName # Switches branches.
'' -b '' # Creates a branch while switching to it.

### Config ###
git config configValue # Displays the value of configValue
git config --global configValue # Setting the value of configValue
```

### Config Variables

- `user.name`: Name of user.
- `user.email`: Email of user.
- `core.editor`: Main editor git invokes.
