# Unix-Shell

## Naming

- Absolute Path: Begins at root. Starts with '/'.
- Relative Path: Begins at current dir. Starts with nothing.
- `\`: Escape character.
- `.`: Current dir.
- `..`: Parent dir.
- `~`: Home dir.

## Commands

```shell
### Basic Movement & File Manipulation ###
cd dirname # Change Directory to dirname.
mv pathname1 pathname2 # MoVes pathname1 to pathname2. Can also be used to rename files.
cp pathname1 pathname2 # CoPies pathname1 to pathname2.
rm filename # Removes filename.

mkdir dirname # MaKes new Dir.
rmdir dirname # ReMoves empty Dir.

ls [path] # LiSts the contents

### Changes Access Modifications ###
chmod {u|g|o|a}{+|-}<access modifications> filename # Adds or removes access permissions to the filename to the specified group.

### Misc. Commands ###
diff filename1 filename2 # Prints the differences between filename1 and filename2.
script filename # Creates a log of console commands at the specified filename

### Pagers ###
more filename
less filename
```

### Andrew File System (NCSU)

- **ACL:** Access Control List. All the access permissions you can specify.
  - **`a` (administer):** Change the entries on the ACL.
  - **`d` (delete):** Remove files and subdirectories from the directory or move them to other directories.
  - **`i` (insert):** Add files or subdirectories to the directory by copying, moving or creating.
  - **`k` (lock):** Set read locks or write locks on the files in the directory.
  - **`l` (lookup):** List the files and subdirectories in the directory, stat the directory itself, and issue the fs listacl command to examine the directory's ACL.
  - **`r` (read):** Read the contents of files in the directory; issue the ls -l command to stat the elements in the directory.
  - **`w` (write):** Modify the contents of files in the directory, and issue the UNIX chmod command to change their mode bits.

```shell
fs la [path] # Lists the Access permissions for the current directory or specified directory.
fs sa pathname unityID accessPermissions # Sets Access permissions for the specified user and pathname.
fs lq path # Lists the Quota of how much space is available in the current path.
fs mkm dirname users.unityID.backup # MaKes a Mount of the backup partition of the user's unityID at the specified dir.
fs rmm dirname: # ReMoves the Mount at the specified dirname.
```

![AFS File Tree](media/Unix-Shell/AFS-tree.png)

## Shortcuts

```no-highlight
C-c: Stop command.
C-z: Pause command.
  fg: Brings command to the ForeGround.
C-d: End of line character.
tab: Auto-complete.
tab x2: Show all possibilities.
```
