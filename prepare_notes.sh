#!bin/bash

# DEPRECATED

SOURCE="../notes/"
DESTINATION="../Paper_Range/_posts/"

# Goal: prepare notes for Jekyll Site automatically
# Requirements: $SOURCE and $DESTINATION must be in same parent dir

# Copies all markdown notes files to `Paper_Range/_posts` and
# adds header necessary for Jeckyll by reading date and title.
for file in ${SOURCE}*/*.md
do
    # reads and parses first line
    read first_line < $file
    date=${first_line:(-11):10}
    title=${first_line#"# "}
    title=${title% \(*}

    # sets new file path
    new_file="${DESTINATION}${date}-${title/ /-}.markdown"

    echo $new_file

    # if the new file currently exists, delete it
    if [ -f $new_file ]; then
        rm $new_file
    fi

    # adds header from read information
    exec >> $new_file

    echo "---"
    echo "layout: post"
    echo "title: \"${title}\""
    echo "date: $date"
    echo "categories: Notes"
    echo "---"
    tail -n +2 $file
done
