* **Long Run Aggregate Supply (LRAS):** Determined exclusively by the factors of production. That's why its vertical for GDP.
  * Only changed by shocks to factors of production.
    * e.g. Climate changes in agrarian economy. New techonology improves production.
* **Short Run Aggregate Supply (SRAS):** Before everyone realizes that the money supply and thus its value has changed.
  * Because prices don't immediately change.
