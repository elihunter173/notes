# Monetary Policy and Central Banking

## US Federal Reserve

* **Federal Reserve (Fed):** The bank for banks. US Controller of monetary policy.
  * Committe of 5 members rotates among 12 people.

## Theories of Money

* **Money:** A widely accepted payment method.
* *Characteristics of Money:*
  * Store of value: The value is fairly constant and people truly believe that it has value.
  * Unit of account: The value is widely agreed upon across people.
  * Medium of exchange: The unit can be easily transferred between people.
* **Money Supply:** The amount of money available in total by society.
  * Affected by...
    * How much money is printed
    * The interest rate (if it's high, then banks "generate" money)
* **Money Velocity:** The amount of time a unit of money is spent in a given time period.
  * Affected by...
    * Fear/Anticipation of future
    * Monetary systems. (How easy is it spend?)
    * *The behavior of people.*
* $P (Prices) * Y (GDP) = M (Money Supply) * V (Money Velocity)$
* $Inflation + GDP Growth Rate = Money Supply Growth + Money Velocity Growth$

* **Monetary Base:** Currency + Reserves held by banks at Fed.
  > Controlled by Fed
* *M1:* Currency + Checkable deposits
  > Missing Reserves held by banks because that is not truly in society
  > Not controlled by central bank!
* *M2:* M1 + Savings deposits + other things
  > More broad. What people *truly* have

## Reserves
* **Fractional Reserve Banking:** Banks hold only a fraction of their deposits. The rest they lend out.
  * Allows for money creation by private banks
* **Reserve Ratio (RR):** A measure of how much money banks loan. ($\frac{reserves}{deposits}$)
  * Fed can mandate this.
* **Money Multiplier:** How much money gets "multiplied" for each amount saved/deposited. ($\frac{1}{RR}$)
* $\Delta Money Supply = \Delta MB * MM$

## IDFK
* Budget Deficit = Government Spending - Tax Revenue
* **Public Debt:** The sum of all past deficits.

## Difficulties
* Cannot know current data. (It takes time to collect these things)

## Effects

* **Open Market Operations:** Buying/selling bonds. Basically, they either inject money into the economy or take it out.
  * Buying bonds: People give money to the FED, thus taking money out of the economy.
  * Selling bonds: People get money from the FED, thus injecting money into the economy.
* **Crowding Out Effect:** Changing government spending increases (or decreases) GDP *less* than amount it's spending.
  * The government spending "crowds out" private business and consumer interactions.
  * Society is *hurt* or at least not helped.
* **Multiplier Effect:** Changing government spending increases GDP *more* than the amount its spending.
  * The government spending's effec is "multiplied" (i.e. it increases the capability of private business and consumer interactions).
  * Society is *helped*.
