# CSS

- *Cascading Style Sheets*
- **Goal:** create stylesheets for website(s)
- [CSS Validation Service](https://jigsaw.w3.org/css-validator/)

## Colors

- additive 24 bit color normal
- written as hex codes (`#rrggbb[oo]`)

## Distances

- pixel (px): the smallest unit a display can display
- percentage unit (%): percentage of size based off containing element's font size
    - if no container, default font size used
    - for text
- em unit: width of a character based off container's font

## HTML Attributes

- `style=`"styling information"
    - "styling information" is in form `"style_attribute1: value1; style_attribute2: value1;"`
        - semicolon optional at end

## Stylesheets

- **Usage:** put `<link href="stylesheet_path" rel="stylesheet" type="text/css" />` somewhere in head of file

### Syntax

```
/* this is a block of CSS rules */
selector {
  property: value [value2] [...];
  property: value [value2] [...];
}

/* this is another block */
selector {
  property: value [value2] [...];
  property: value [value2] [...];
}
```

#### Options for Selectors

- `tag`: tag that these rules will apply to
- `#id_value`: creates a *unique* ID that can be used in HTML to reference this selector
    - `<tag id="id_value">`
- `.class_name`: creates a *resuable* selector "class" that can be used in HTML to reference this selector
    - `<tag class="class_name">`

## Useful Charts

### Text Chart

![Text](media/css/text_chart.png)

### List Chart

![List](media/css/list_chart.png)

### Styling Elements Chart

![Styling Elements](media/css/styling-elements_chart.png)