# XHTML 5

- *eXtensible Hypertext Markup Language 5*
- **Goal:** offer basic, universal webpage (and more) design
- [XTHML 5 Validator](https://validator.w3.org/)

## **Tag:** *what XHTML uses to categorize text*

- **Paired Tags:** `<tag>sample text</tag>`
    - can have things nested inside!
- **Self-closing Tags:** `<tag sample info />`
- **Attributes:** information put inside a tag to give more information
    - `<tag attribute="attribute value"> sample text </tag>`
    - `<tag attribute="attribute value" />`
    - ex: URL links, images

### Text Tags

- `<p>`: paragraph
- `<h#>`: heading # (1-6)
- `<br/>`: line break
- `<hr/>`: horizontal line
- `<strong>`: denotes text as strong (bold)
- `<em>`: denotes text as emphasized (italic)
- `<ins>`: denotes text as inserted (underlined)
- `<del>`: marks text as deleted (strikethru)
- `<sub>`: marks text as subscript
- `<sup>`: marks text as superscript

### List Tags

- `<ul>`: unordered list
- `<ol>`: ordered list
- `<li>`: list item tag. required for all individual list entries

### Table Tags

- `<table>`: container for rest of tags
- `<tr>`: creates a table row for coulmn entries to be put in
- `<td>`: for a single table data entry
- `<th>`: creates table header same as `<td>` but centers and bolds text

### Structural Tags

- don't change visual information, just create structural containers for later reference. **containerization**
- `<div>`: describes generic container with no visual meaning
- `<span>`: describes generic in-line container with no visual meaning
- [specific structural tags](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Document_and_website_structure)

### Miscellaneous Tags

- `<img attributes />`: shows images
    - `src=`"path to image"
    - `alt=`"text for image if can't be loaded"
- `<a attributes>`: anchor. makes something a clickable link
    - `href=`"URL"
        - `mailto:email` for URL opens email client to compose email to person
    - `target=`"where to open link"

## Comments

```html
<!-- Comment Body -->
```

## Mandatory Skeleton

```html
<!DOCTYPE html> <!-- declares document as html>

<html xmlns="http://www.w3.org/1999/xhtml">

    <!-- the head doesn't appear, just provides information for
         the browswer, search engine, etc. -->
    <head>
        <!-- meta includes misc. information (encoding charset, author, publishing date, etc) -->
        <meta charset="UTF-8" />
        <!-- title for search engine and browser -->
        <title>Your title goes here.</title>
    </head>

    <body>
        <!-- The content of your webpage goes here. -->
    </body>

</html>
```