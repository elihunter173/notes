# Jekyll

- [Guide](https://www.taniarascia.com/make-a-static-website-with-jekyll/)
- **Jekyll:** *A static site generator.*
- **Static Site:** A website that is created from hardcoded HTML.
- **Static Site Generator:** A very lightweight "CMS" (Content Management System).

## Jekyll File System

- `_site/`: The distribution dir. Where the static site is generated.
    - All files and dirs in main dir will be placed in `_site/` as is
- `_sass/`: the dir for Sass partials. All files here begin with '_'.
- `_includes/`: the dir for all files that show up on every page.
- `_layouts/`: the dir that contains the layout that content will conform to.