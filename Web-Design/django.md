# Django Notes

*What is Django?* A python based web develop library and tool

## Methods

- `django.urls.include()` chops off whatever part of url matched to that point and sends rest to URLconf for more processing
    - makes it easier to plug-and-play urls. *can move around easier*

## Embedded Python

inside of a html file, use the following syntax:`{\% dummyCommand dummyArg \%}` (backslashes just fix Jekyll build error)

### Common Commands