# Arrays

- **Array:** A immutable, ordered list of data. Each item in an array has a specific index.

## Components

- **Index:** The position of a value in an array from the beginning. Starts at 0.
- **Values:** The actual values inside of the array.

## Java

- Arrays are reference objects
  - Variables are merely addresses to the object in memory.
  - Changes inside of methods change the object outside of the method

### Example

```java
int[] array = { 1, 2 };
swap(array, 1, 2);

void swap(int[] array, i, j) {
    int temp = array[i]; // Set up temp variable to save old i
    array[i] = array[j]; // Set new i to j
    array[j] = temp; // Set new j to old i
}
```

### Quirks

- Number type arrays are initialized with 0s.
- Boolean type arrays are initialized with false.
- Object type arrays are initialized with null.

### Common Attributes

- `length`: The length of the array.

### Common Methods

- All Arrays method called Arrays.methodName(params).
- `binarySearch(dummyArray, value)`: Returns the index of that value in a *sorted* array.
- `copyOf(dummyArray[, length])`: Returns a copy of the array truncated to be that length.
- `equals(dummyArray1, dummyArray2)`: Returns true if both arrays contain the same elements in the same order.
- `fill(dummyArray, value)`: Fills the array with `value`.
- `sort(dummyArray)`: Arranges the elements of the passed array.
- `toString(dummyArray)`: Returns the array represented as a string
- `sort(dummyArray[, comparator])`: Sorts the objects in the array. If a comparator is provided, then the objects are sorted in terms of that comparator.

### Syntax

```java
// Array object declaration/creation
type[] name = new type[int length if lenth > 0];
// Array object initialization
name = {value[, value, ...]};

// Can be combined
type[] name = {value[, value, ...]};
```

### Exceptions

- `ArrayIndexOutOfBounds`: An exception thrown when trying to access an array's value with an invalid index. Index is out of the range of the array.

### Comparators

- A separate class that implements Comparator<ObjectName>.
- Must have a compare method thus:

```java
import java.util.Comparator;

// NB: There is no constructor for this comparator. This uses default constructor.
public class FooComparator implements Comparator<Foo> {

    public int compare(Object object1, Object object2) {
      // Returns negative if object1 < object2
      // Returns positive if object1 > object2
      // Returns zero     if object1 = object2
    }

}
```
