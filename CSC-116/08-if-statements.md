# If-Statements

## Syntax

```java
if (condition) {
    statements;
    ...
 } else if (condition) {
    statements;
 } else {
     statements;
 }
```

*else is optional!*

## Conditions

- Order: Arithmetic Operators, Relational Operators, Logical Operators

### Relational Operators

- `<` is less than
- `>` is greater than
- `=>` is greater than or equal to
- `=<` is less than or equal to
- `==` is equal to
- `!=` is not equal to

**Note:** *always return booleans, only works on primitive values*

### Logical Operators

- `&&` is conditional AND
  - short circuit evaluation. only evaluates the necessary values (starting left)
- `||` is OR
  - short circuit evaluation. only evaluates the necessary values (starting left)
- `!` is NOT
- `^` is XOR
- `&` is AND
  - evaluates both
- `|` is OR
  - evaluates both

### De Morgan's Law

```pseudo-code
!(p OR q) = !p AND !q
x < y = x >= y
```

**Truth Table**
![Truth Table](media/08-If-Statements/truth-table.png)
