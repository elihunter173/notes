# Brainstorming 11-27-2018
* **Goal:** Keep track of unofficial transcript.
* Student Records include courses, grades, name, unity ID, phone number, address
* **Stakeholders:** Student, Instructor, Academic Administrator, Technical Administrator

## Functional Requirements
* **Formal:** The student shall be able to view their current and past classes.
* **Shorthand:** View their current and past classes.
  > Underneath the stakeholder type heading

### System
* Protect the privacy of student information.
  * e.g. other students, guests

### Students
* View their current and past classes.
* View their current and past grades.
* View the class roster of their enrolled classes, including names, unity IDs.
* NOT Edit their own grades.
* Edit their own personal information, without vulgarity.
* Search through the names and unity IDs of all students.

### Instructors
* Add a student to a class.
* Add and edit the grades of a student in their class for that class.
* See a list of all their classes which they instruct, including the class roster and grades.
* NOT View or edit the personal information of the students in their class.

### Academic Administrator
* View the grades and classes of all students in their deparment
* Check that a student has passed graduation requirements.

### System Administrator
* Edit courses, grades
* Reset the password of a student.
* NOT Set the password of a student.

## User Stories
* A student logs into the system, providing their unity ID and a password, and sees a list of their classes. From there, they select one and see a list of all their personal grades in that class. Then, they see a list of all of their current grades as well as a list of all students enrolled in that class.
