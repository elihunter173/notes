# Graphical User Interfaces (GUIs)

## Main Parts

- **Model:** The information that is being displayed.
- **Controller:** What is controlling/changing the model.
  - Uses *mutators* and *action events*
  - This should be able to be replaced entirely!
- **View:** What is displaying the model.
  - Uses *accessors*

- **A GUI is driven by a user action without prompt**

## Java

- **JFrame:** A class that helps support GUIs.

## Misc

- **NO MAGIC NUMBERS**
