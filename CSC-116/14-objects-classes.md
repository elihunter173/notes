# Objects & Classes

- **Object:** A collection of variables and methods.
- **Class:** A pattern for objects. Contains variables and methods the objects need to define. (methods normally predefined)
  - **Static:** A class variables & method. Are always defined even if no object instance exists.
  - **Extends:** Means that this class is a subclass (child) of the class it extends and has almost all its methods.
  - **Implements:**
- **State:** The current information and methods in the object.
- **Mutability:** Whether an object can be changed or not after creation
  - **Immutable:** Cannot be changed.
  - **Mutable:** Can be changed.

## Usage

- **Constructor:** The method that sets up all the necessary variables (attributes), allocates memory, and creates the object given a certain class.
  - **Default Constructor:** A constructor that takes in no arguments and simply sets up the object in memory. This always exists even if not explicitly defined.
- **Accessor/Getter:** Returns a specific attribute from the object.
- **Mutator/Setter:** Changes the state of the object. Oftentimes throws exceptions when input is bad.

### Recommended Methods

- `Object.equals()`: Returns whether the Object is equal. What constitutes "being equal" is decided by the developer.
- `Object.toString()`: Returns some information about the Object as a String.

### Java Syntax

```java
public class Car {

    // Attributes //
    // These are declared as private because, if you don't, there is a chance things get fussed up
    // by the user
    private String make;
    private double mileage;
    private double capacity;
    private double fuelRemaining;

    // NB: no return type, name same as class
    public Car(String make, double mileage, double capacity) {
        // `this` returns to attributes of the current object
        // Here it is because we are defining it.
        this.make = make;
        this.mileage = mileage;
        this.capacity = capacity;
        this.fuelRemaining = capacity;
    }

    // Accessors //
    public double getMake() { return make; }
    public double getMileage() { return mileage; }
    public double getCapacity() { return capacity; }
    public double getFuelRemaining() { return fuelRemaining; }
    public double getRange() { return fuelRemaining * mileage; }

    // Mutators //
    public void drive(double miles) {
      return fuelRemaining - (miles * mileage);
      // throws exception if miles negative or fuelRemaining will be negative
    }
}
```

### Special notes

- Classes don't need a main method, but they can be created for simple white-box testing.
