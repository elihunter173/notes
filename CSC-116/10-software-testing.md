# Software Testing

- **Goal of Testing:** find faults in the software.
  - saves money
  - saves lives
  - improves quality of software

- **Mantra:** *"Test early and often."*
- *no amount of testing can prove your software to be fault free*
- *write tests before testing*

## Types of Testing

- **Static Testing:** finding faults during compilation.
- **Dyanamic Testing:** finding faults during runtime.
- **Unit Testing:** testing small part of program.
- **System Testing:** testing full part of program.

## Black Box Testing

- **Black Box Testing:** tests the program as a whole / as a user. disregards internals and
  just make sure that it outputs correctly for given input. *treats program as blackbox*
  - anyone can do, even if don't understand code.
  - *system testing*
- **Testing Requirements:** what the code has to do.
- **Equivalence Classes:** what type of output the code should return for a given range of inputs.
- **Boundary Value Analysis:** making sure that the code outputs correctly even at the boundary of equivalence classes

## White Box Testing

- **White Box Testing:** tests individual parts of program, knowing what was inside
- **Equivalence Classes:** what type of output the code should return for a given range of inputs.
- **Boundary Value Analysis:** making sure that the code outputs correctly even at the boundary of equivalence classes
- **Basis Path Testing:** tests all valid logical paths in a method. *all possibilities*

### Control Flow Diagram

- shows the logical flow of program
- *Diamonds:* decisions
- *Rectangles:* program statements
- break apart compound statements!

### Cyclomatic Complexity

- measures complexity of program
  - the number of independent paths in program

![Cyclomatic Complexity](media/10-Software-Testing/testing-diagram.png)

### JUnit Testing

- JUnit is a testing framework that reduces complexity
- tests each of the individual units

#### Naming Conventions

- Test Files: `test/<ProgramName>Test.java`
- Test Methods: `test<MethodName><TestDescription>()`

#### JUnit Annotations

- `@Test`: identifies a test method
- `@Before`: identifies method that runs before all other test methods
  - ensures all have same starting conditions

#### Assert Statements

- JUnit tests must have assert statement
- assert statements "check" actual vs expected results

```java
assertTrue(String message, Boolean method);
assertFalse(String message, Boolean method);
assertEquals(String message, PrimitiveType expected, PrimitiveType method);
```

## Test Cases

- What is in a Test Case?
  - unique identifier (PascalCase)
  - input
  - expected output
  - result

![Testing Diagram](../media/10-software_testing/testing_diagram.png)
