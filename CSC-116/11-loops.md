# Loops

- **Definite Loop:** A loop that executes an obvious amount of times.
- **Indefinite Loop:** A loop that executes a non-obvious amount of time.

## Main Components of Loops

- **Iterator:** What is being changed.
- **Test Condition:** What must be true to execute body of loop once more.

## Decision Diagrams

![While Loop Decision Diagram](media/11-Loops/while-loop-decison-diagram.png)

### Key

- *Square:* An action.
- *Diamond:* A decision.

## While Loops

```java
while (condition) {
    statements;
    ...
}
```

- While condition is true, execute statements.

## Do-While Loops

```java
do {
    statements;
    ...
} while condition;
```

- Execute statements. Then, if condition is true, execute them again and repeat this sentence until condition false.
