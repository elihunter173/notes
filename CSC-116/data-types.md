# Data Types

## Syntax

```java
// Declaration //
primitive dummyPrimitive; // For primitives
Object dummyObject; // For Objects

// Assignment //
dummyPrimitive = primitiveValue;
dummyObject = anotherObject;

// Initialization (Declaration + Assignment) //
primitive dummyPrimitive = primitiveValue;
Object dummyObject = anotherObject;
```

## Java 8's Primitives / Primitive Types

[Source](https://en.wikibooks.org/wiki/Java_Programming/Primitive_Types)

| Types     | Size (bytes) | Min Value         | Max Value                              | Precision                                                     | Example             |
| --------- | :----------: | :---------------: | :------------------------------------: | ------------------------------------------------------------- | ------------------- |
| `byte`    | 1            | -2<sup>7</sup>    | 2<sup>7</sup> - 1                      | From +127 to -128                                             | `byte b = 65;`      |
| `char`    | 2            | 0                 | 2<sup>16</sup>                         | All Unicode Characters                                        | `char c = 'A';`     |
| `short`   | 2            | -2<sup>15</sup>   | 2<sup>15</sup> - 1                     | From +32,767 to -32,768                                       | `short s = 65;`     |
| `int`     | 4            | -2<sup>31</sup>   | 2<sup>31</sup> - 1                     | From +2,147,483,647 to -2,147,483,648                         | `int i  = 65;`      |
| `long`    | 8            | -2<sup>63</sup>   | 2<sup>63</sup> - 1                     | From +9,223,372,036,854,775,807 to -9,223,372,036,854,775,808 | `long l = 65L;`     |
| `float`   | 4            | 2<sup>-149</sup>  | (2-2<sup>-23</sup>) * 2<sup>127</sup>  | From 3.402,823,5 E+38 to 1.4 E-45                             | `float f = 65f;`    |
| `double`  | 8            | 2<sup>-1074</sup> | (2-2<sup>-52</sup>) * 2<sup>1023</sup> | From 1.797,693,134,862,315,7 E+308 to 4.9 E-324               | `double d = 65.55;` |
| `boolean` | ---          | ---               | ---                                    | true, false                                                   | `boolean b = true;` |
| `null`    | ---          | ---               | ---                                    | ---                                                           | ---                 |

### Families

- **Integers:** `byte`, `char`, `short`, `int`, `long`.
- **Floating Point:** `float`, `double`,
- **Other:** `boolean`, `null`,

## Java Strings

- A weird in-between of Objects and primitives.
  - Is declared like primitive and has some primitive operators, but isn't equated like that and is typed like an Object.
- **Concatenation (`+`):** Concatenates the String before and the String after.
  - Same as `before.concat(after)`

## Mathematical Operators

- **Addition (`+`):** Adds the number before and after.
- **Subtraction (`-`):** Subtracts the number after from the number before.
- **Multiplication (`*`):** Multiplies the number before and after together.
- **Division (`/`):** Divides the number before by the number after.
- **Modulo (`%`):** Returns the remainder when dividing the number before by the number after.
- **Exponentiation:** In the Java Math library. `Math.pow(base, exponent)`

### Order/Priority of Operations

- PEMDAS.
- Multiplication = Division = Modulo > Addition = Subtraction > Assignment

### Promotion

- **Promotion:** When a less precise data type interacts with a more precise data type, the result is *promoted* to the more precise data type.
- **Precision:** String > Floating Point > Integer.
  - Within families, the more memory a value takes up, the more precise it is.
