# Waterfall Software Development
* Very ordered and sequence dependent.
* Older and generally less preferred compared to agile.
  * Why? Agile constantly provides software from the customer and requests feedback, and customers often don't know what they _really_ want
* **Defects:** Issues that cause problems.
  * The cost of defects increases further down the line.

## Waterfall Phases
1. Requirements/Analysis
2. Design
3. Implementation
4. Testinging
5. Maintenance

## Requirements/Analysis
* **Goal:** Understand customer requirements and software system.
  * What! Not how.
* **Functional Requirements:** What the software must actually do.
  * e.g. The application allows clerks to check out DVDs.
  * The [stakeholder type] shall be able to [capability/function]. The system shhall allow the [stakeholder type] to [capability/function]. The system shall [capability/function].
* **Nonfunctional Requirements:** Determines the quality of the actions, what devices the software must support, what the software requirements are, etc.
  * e.g. The application must load within 2 seconds.
* _N.B._ These are difficult to get right and can change.
* **Artifacts:** Documentation left behind for future developers and users. (requirement documents, use cases, user stories)

### User Story
* A sequence of events that happen with the software.
  * e.g. The user logs into AFS, is asked for their login ID and their password, then they are put in their home directly, and then they access their desired text file in their home directory.
* Helps keep developers on track and focused.
* Provide very concrete examples of what must happen.

## Design
* **Goal:** Gain a broad understanding of the software and how it will be designed. _The design depends on it's function!_
  * What are the classes? How will they interact?
* **Artifacts:** Design documents, class diagrams, UML diagrams, and similar.
* _This is a bridge between requirements and implementation._

### Associations
* **Aggregation:** A object has at least one instance of another object.
* **Composition:** A object is made (almost) entirely of another object.
* **Association:** Two objects are related to each other as peers.
* **Generalization:** A object is a specific type / subclass / child of the more general object.
* **Dependencies:** A object needs to have another class/object to function.

### UML (Unified Modeling Language)
* Models object-oriented software.
* Convergence of three earlier modelling languages:
  * OMT
  * OOSE
  * Booch
* Overseen by Object Mentor Group (OMG).

#### Associations
* **Aggregation:** Shown with a hollow diamond on the side that aggregates the object.
* **Compositition:** Shown with a solid diamon on the side is a composition of the object.

#### Classes
```uml
ClassName
---------
Attribute: type (type is less important)
---------
+publicOperation(paramName: type, ...): return-type
-privateOperation(paramName: type, ...): return-type
#protectedOperation(paramName: type, ...): return-type
```

```uml
Student
-------
- name: String
- gpa: double
- studentId: long
- creditHoursCompleted: int
-------
+updateGPA(...): void
```

### Tips
* Nouns are Classes.
* Verbs are Methods.
* Consider all singulars and plurals the same (probably).
* Discard all synonyms.
* Discard all unimportant adjectives.
* See how nouns are related. (For subclasses, interfaces, etc.)
  * Can these classes be generealized? E.g. Can't I consider a `Student` and a `Professor` both a `Person`.

