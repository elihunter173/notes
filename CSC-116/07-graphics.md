# Graphics Objects

## DrawingPanel: window on the screen

- "canvas" to draw on
- **Creation:** DrawingPanel namedPanel = new DrawingPanel(width, height);
- *Notes:* coordinates start top left, x+ ->, y+ |v

### DrawingPanel Methods

- `clear();`: clears drawing panel

## Graphics

- "pen" to draw shapes in
- **Creation:** `Graphics pen = namedPanel.getGraphics();`
- **Package:** `java.awt.Graphics`
- last drawn object has highest priority

### Graphics Methods

- `setColor(Color);`
- `drawLine(x1, y1, x2, y2);`
- `drawRect(x1, y1, width, height);`
- `drawOval(x1, y1, width, height);`
- `drawString(string, x, y);`
- `fillRect(x1, y1, width, height);`
- `fillOval(x1, y1, width, height);`

## Color

- color to draw in
- **Creation:** Color namedColor = new Color(R, G, B) 0 <= R,G,B <= 255
- **Constants:** normally named colors, do Color.NAME