# Conditions

- **Precondition:** condition that must be true, before method is called, for method to work properly
- **Post-condition:** condition that must be true after method is run, given precondition was met
- *two sides of same coin*
    - "What is the domain of this method?"
    - "What does this method need to work?"

## Errors

- **"Throw early, catch late"**
- **Throw:** what says there is an error
- **Catch:** handles the thrown error
    - ex: tell the user about the error
- **Try-Catch Block:**
    - **try:** run code anticipating error
    - **catch:** handle error if thrown in try block
    - *Why?* Because otherwise the user gets a gross stack trace
- **Stack Trace:** list of errors in order they occured with file names and lines error occured in. very user-unfriendly but highly informational

### Syntax

**Throw:**

```java
throw new ErrorName(errorMessage);
```

**Catch:**

```java
catch (DummyErrorType dummyError) { ... }
```

**Try-Catch Block:**

```java
try {
    if (i < 2) {
        throw new Error(errorMessage);
    }
} catch (DummyErrorType dummyError) {
    doStuff();
    System.out.println(dummyError.getMessage());
}
```