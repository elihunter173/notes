# Methods

## Java Syntax

```java
// Declaring a Method
accessModifier [modifiers] [ReturnType] methodName([ParameterType parameterName, ...]) {
    statement;
    ...
}

// Calling a Method
ClassName.methodName([args, ...]); // ClassName is optional if the method is static and used within the class
```

## Anatomy

- **Return Type:** What type of object the method will return.
  - *`void`:* No return.
- **Access Modifiers:** Determines what classes can access this method.
  - *`public`:* All classes can access this.
  - *`private`:* Only the owning class can access this. *(Can't be used on classes! Why would you want to?)*
  - *`default`:* Only this class and classes in this class's package can access this. *Written by including no accessModifier.*
  - *`protected`:* The same as default, but subclasses can also access it.
  - That's it!
- **Arguments / Parameters:** The objects passed to the method and declared within its scope.
- **Signature:** The method's name and parameters.

## Purpose of Methods

- Allows the creation of "black-boxes" within a program. Allows code reuse, more legible code, and easier maintenance.
