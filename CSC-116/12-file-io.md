# Files

- Unix makes cross platform files I/O way easier. standardization!

## Parts of File

- Name: The name of the file including the path.
- Location: The path to the file.
- Access: The access rights to the file
  - `-rwxr-xr-x`, `dr-xr-xr-x`, etc.

## File Class

`java.io.File`

- Name is `String`
- Location is `File`
- Access is `File`
- Has various methods for checking access, location, reading, etc.

### Methods

- String `getName()`: returns String of file name.
- boolean `canRead()`: returns whether file is readable
- boolean `exists()`: returns true if the file exists.
- `delete()`: deletes file.
- `length()`: returns number of bytes in file.
- `renameTo("newName")`: changes name of file.

## Scanners & Files

`java.util.Scanner`

- `Scanner()` constructor can take in a file or string.
- Input Cursor keeps track of where Scanner is in InputStream. It starts at the beginning and advances to past the next token whenever `next___()` is called.

### Tokens

- Can't combine tokens with double quotes.
- Cursor can skip input by doing `.next()`  without saving value.
- Double tokens don't need to have decimal point.

### Lines

- `String nextLine()`: reads entire line and returns string.
- `boolean hasNextLine()`: sees if the next line exists.

## PrintStream

`java.io.PrintStream`

- Allows you to print things to a location (File, console (System.out PrintStream))

### Methods

```java
PrintStream name = new PrintStream(new File("file name"));
```

- Writes new file regardless
- `.println()`: prints a line to the PrintStream

#### Example

```java
PrintStream out1 = System.out;
PrintStream out2 = new PrintStream(new File("data.txt"));
out1.println("Hello, console!");   // goes to console
out2.println("Hello, file!");      // goes to file
```

## Related Notes

- `System.in` is a type of `File` (location is console)
- `System.out` is a type of `PrintStream`

## Exceptions

- `Exception`: object representing a runtime error
  - `Checked Exception`: exception which must be dealt with for compilation

### Handling Exceptions

- Use try-catch block.

**`throws`:** Keyword in methods header that states that method may throw specific exception, but does not handle it

```java
public static void dummyMethod() throws FileNotFoundException {
    Statement;
    ...
}
```
