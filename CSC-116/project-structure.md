# A Description of Project Directories

```ascii
Project#/
|-- src/ : Storage of .java files (source).
|-- bin/ : Storage of .class files (binary).
|-- lib/ : Storage of .jar files (library).
|-- test/ : Storage of testing .java files (testing).
|-- project_docs/ : Storage of all project related non-coding files.
```
