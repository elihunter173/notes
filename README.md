# Notes

This is a repo for Eli W. Hunters notes in markdown.

## Conventions

- Directories are in Train-Case
- Note files (markdown) are in spinal-case.

```no-highlight
/
├── Subject-Name: A subject specific directory.
│   ├── media: A directory for all the non-markdown files.
│   └── example-notes.md
```
