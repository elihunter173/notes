# Resume Talk

* **Carrie Christiano**, Resume Writer

## Big Ideas

* **Value Proposition / Brand:** What can I do to help?
  * Focus on how you can help the company, network, have a purpose/reason for all communication, organize! (spreadsheets, reminders, etc), keep in contact but don't harrass recruiters.

## Job Searching

* Be proactive.
* Network.
* Make lists for companies. (_must-haves, nice-to-haves, not-at-alls_)
* Have an elevator speech prepared.
* Show your knowledge and interest in the company.

### The "Ways In" to a Company

* **Networking & Recruiters:** Develop relationships with them.
  * _Agency Recruiters:_ Mercenary recruiters. Try to find immediately ready/experienced hires.
  * _Company Recruiters:_ Try to find new recruits.
* **Internet Application:** The void. Very easy but not super successful.
* **Direct Marketing:** Make yourself known. Have a website. Be famous.
* **Hidden Job Market:** Jobs that haven't officially been posted yet.

## Resume

* **DON'T GIVE RESUME UNLESS ASKED!**
* A resume is a marketing brochure.
  * Don't spend too much time on it if you don't know someone will read it. (e.g. online resume submissions)
  * If you're not a designer, don't worry too much about the design.
* Write first, format and prune later.
* _List:_ hard skills, soft skills, knowledge/skills, _experience_, _accomplishments_
* Focus on _accomplishments_ over skills.
* Have a brand!
* Brag about yourself! (reasonably)

### Writing Techniques

* Past tense. No subject.
* Spell check and proofread.
* Use .pdf or plain text.
* Don't mention references. _(it's assumed!)_
* No color.
* No street addresses. _(you may live in a shitty or nice part of town)_
  * Just zip codes are okay.

#### Order

1. Object/Summary
  * Summary: Generally better.
  * Objective: Useful when (re)entering field.
2. Experience & Accomplishments
  * _C:_ Challenge
  * _A:_ Action/Activity
  * _R:_ Result
3. Education & Training
4. Professional Affiliations
5. Community Service
6. Publications, Presentations, Patents, and Awards

## Cover Letter

* Focus on the company, not yourself.
* Show genuine interst

## Online Systems

### Online Presence

* Personal website.
* LinkedIn: A full profile.
* JibberJabber: Tracking contacs.
* PechaKucha: Short powerpoint promo.
* O\*Net Code Connector: Search for job connects.

#### Social Media

* Keep your social media clean.
* Show what you do in your free time.
* Look at the company's Twitter. See if you'd be a good fit!

## Interviewing

* Interviews are often situational-based
